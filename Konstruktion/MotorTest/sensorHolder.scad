innerH = 50;
motorL=16;          //motor lenght
motorDia=7;         //motor diameter
mTol=0.3;
plateLength = 70;
plateWidth = 40;
plateThickness = 10;
nutLength = 50;
nutThickness = 3;

translate([0,0,(nutThickness-mTol-0.2)/2])t(3,10);
translate([10, 0, (2*nutThickness)+7.5])plate();
translate([0,0,6])diesdas();

module plate(){
  $fn = 20;
  difference(){
    cube(size=[30, 8, 5], center=true);
    translate([19/2, 0, 0]) cylinder(r=3/2, h=10, center=true);
    translate([-19/2, 0, 0]) cylinder(r=3/2, h=10, center=true);
    cube(size=[8.5, 3.5, 10], center=true);
  }
}

module tNut(thickness, length){
  cube(size=[length, 2*thickness, thickness], center=true);
  translate([0, 0, thickness]) cube(size=[length, thickness, thickness], center=true);
}

module t(thickness, length){
  cube([length, 2*thickness-mTol-0.55, thickness-mTol-0.35], center=true);
  translate([0, 0, thickness])cube(size=[length, thickness-mTol-0.55, thickness+mTol+1], center=true);
}


module diesdas(){
hull(){
    translate([0, 0, 0])cube(size=[10, nutThickness-mTol-0.55,1], center=true);
    translate([0, 0, 5])cube(size=[10, 8,1], center=true);
}
}

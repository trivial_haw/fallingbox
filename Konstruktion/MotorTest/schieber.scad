innerH = 50;
motorL=16;          //motor lenght
motorDia=7;         //motor diameter
mTol=0.3;
plateLength = 70;
plateWidth = 40;
plateThickness = 10;
nutLength = 50;
nutThickness = 3;

schieber();

module schieber(){
  schieberThickness = 7;
  difference(){
    union(){
      translate([-schieberThickness/2,-plateWidth/2, 2*nutThickness])cube([schieberThickness,plateWidth,40]);
      translate([0, 15, nutThickness/2-0.2]) t(nutThickness, schieberThickness);
      translate([0, -15, nutThickness/2-0.2]) t(nutThickness, schieberThickness);
    }
      translate([0, 0, 10]) rotate([0, 90, 0]) tNut(nutThickness, 100);
      //translate([0, 0, 50])#cube(size=[20, 50, 70], center=true);
  }
}

module tNut(thickness, length){
  cube([length, 2*thickness, thickness], center=true);
  translate([0, 0, thickness]) cube(size=[length, thickness, thickness], center=true);
}

module t(thickness, length){
  cube([length, 2*thickness-mTol-0.55, thickness-mTol-0.35], center=true);
  translate([0, 0, thickness])cube(size=[length, thickness-mTol-0.55, thickness+mTol+1], center=true);
}

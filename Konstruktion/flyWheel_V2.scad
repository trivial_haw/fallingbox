outerD=30;              //Outer Diameter
dHole=5;                //Mountinghole Diameter
height=5;               //heigth of the flywheel

tolerance=0.35;          //Printtolerance to fit balls and motor
dMass=6.1;                //Diameter of the balls



flywheel();

module flywheel(){
    outerRad=outerD/2;
    innerRad=outerRad-(dMass+3);
    rHole=(dHole+tolerance)/2;
    dMass=dMass+tolerance;
    midRad=(innerRad+(outerRad-innerRad)/2);
    difference(){
        union(){
            ring(outerRad,innerRad,height);
            myCross(2*midRad,outerD/10,height);
            cylinder($fn=90,r=rHole+3,h=height,center=true);
        }
        cylinder($fn=90,r=rHole,h=height+1,center=true);
    ballCut(dMass,height,midRad);
    }
}



module ring(rO,rI,h){
    $fn=90;
    difference(){
        cylinder(r=rO,h=h,center=true);
        cylinder(r=rI,h=h+2,center=true);
    }
}

module myCross(l,d,h){
    cube([l,d,h],center=true);
    cube([d,l,h],center=true);
}

module ballCut(d,h,midRad){
    $fn=90;
    rMass=d/2;
    h=h+1;
    for (i = [0:90:360]){
        rotate([0,0,i])translate([0,midRad,0])
            cylinder(r=rMass,h=h,center=true);
      /*  rotate([0,0,i+30])translate([0,midRad,0])
            cylinder(r=rMass,h=h,center=true);
        rotate([0,0,i+60])translate([0,midRad,0])
            cylinder(r=rMass,h=h,center=true);*/
    }
}

outerD=40;              //Outer Diameter
dHole=3;                //Mountinghole Diameter
height=5;               //heigth of the flywheel

tolerance=0.35;          //Printtolerance to fit balls and motor
dMass=8;                //Diameter of the balls



flywheel();

module flywheel(){
    outerRad=outerD/2;
    innerRad=outerRad-(dMass+2);
    rHole=(dHole+tolerance)/2;
    dMass=dMass+tolerance;
    midRad=(innerRad+(outerRad-innerRad)/2);
    difference(){
        union(){
            ring(outerRad,innerRad,height);
            myCross(2*midRad,outerD/10,height);
            cylinder($fn=90,r=rHole+3,h=height,center=true);
        }
        cylinder($fn=90,r=rHole,h=height+1,center=true);
        for (i = [0:90:360]){
            rotate([0,0,i])translate([0,midRad,0])
                ballCut(dMass,height);
            rotate([0,0,i+45])translate([0,midRad,0])
                ballCut(dMass,height);
        }
    translate([0,0,height/2])roundRing(midRad,dMass-5);
    translate([0,0,-height/2])roundRing(midRad,dMass-5);
    }
}



module ring(rO,rI,h){
    $fn=90;
    difference(){
        cylinder(r=rO,h=h,center=true);
        cylinder(r=rI,h=h+2,center=true);
    }
}

module myCross(l,d,h){
    cube([l,d,h],center=true);
    cube([d,l,h],center=true);
}

module ballCut(d,h){
    $fn=60;
    r=d/2;
    sphere(r=r,center=true);
    cylinder(r=r-0.2,h=h+1,center=true);
}

module roundRing(r,dCircle){
    rCircle=dCircle/2;
    rotate_extrude($fn = 40)
    translate([r,0,0])circle(r=rCircle,center=true);
}

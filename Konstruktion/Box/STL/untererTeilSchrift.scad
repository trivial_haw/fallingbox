
difference(){
  box();
  #controlPlate();
}



module box() {
  render()import("untererTeil_v4.stl");
}

module controlPlate(){
  translate([-178/2+1, 0, 0])rotate([0,-90,0])linear_extrude(height = 2){
     rotate([0, 0, 0])translate([12, 40, 0])text("ON", size=5);
     rotate([0, 0, 0])translate([10, 13, 0])text("OFF", size=5);
     rotate([0, 0, 0])translate([24, 40, 0])text("Mode", size=5);
      //rotate([0, 0, -90])translate([-19, 19.5, 0])text("RST", size=5);
     rotate([0, 0, 0])translate([24, 0, 0])text("Reset", size=5);
     rotate([0, 0, -90])translate([4, 36, 0])text("Battery", size=5);
     rotate([0, 0, -90])translate([5, 2, 0])text("♥-beat", size=5);
     rotate([0, 0, -90])translate([30, 2, 0])text("Status", size=5);
     rotate([0, 0, -90])translate([29.5, 36, 0])text("Debug", size=5);
  }
}

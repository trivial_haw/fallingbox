pThickness = 1.5;
pWidth = 4;
pLength = 10;

translate([0, 0, 2.5]) {
  import("Schwungscheibe_30-6-5.stl");
  translate([-10.5, 0, 0]) rotate([0, -90, 0]) Pin();
  translate([10.5, 0, 0]) rotate([0, -90, 180]) Pin();

}

module Pin(){
  union(){
    hull(){
    translate([2.5, 0, 0])rotate([0, -90, 0]) cylinder(r=3.5, h=10, center=true);
    translate([pLength/2+4, 0, pThickness/2]) cube(size=[1, pWidth, pThickness], center=true);
  }
  translate([pLength/2+4, 0, pThickness/2]) cube(size=[pLength, pWidth, pThickness], center=true);
  }
}

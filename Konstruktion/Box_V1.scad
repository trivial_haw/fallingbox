/*--------# To Do #---------
    ~Teensy mount
    ~Motorblocks (Done for Crazyflie7x16)
    ~BoxCap
    ~LiPo mount
    ~Cutout for USB?
    ~Cutout for SD-support
    ~Problems with the magnetic Field of the Motors?
        ->if they're to near by the IMU

---------Design-Rules-------
    ~Try to keep it symmetric
        [in X,Y,Z]
--------------------------*/

choice = 6;

/*--------# Choose #---------
    0)The lower Part of the Box
    1)The Cap of the Box
    2)The motor-mount
    3)The IMU-mount-block
    4)The lower Part of the Box with Motormount
    5)The Simulation-Ears
    6)
---------------------------*/
//Box
innerH=35;                  //Boxheight (inside)

//IMU
/*
imuBlockL=25.4;
imuBlockW=12.7;
imuHeight=1.5;          //to change!!!!
imuBlockH=innerH/2-imuHeight/2;
*/
mountHoleX=5.08;
mountHoleY=7.62;
mountHoleRad=2/2;

//Box-dimension
wall=4;                     //Wallthickness
outerW=(7+26+25+4+wall)*2;                 //Width (Y)
outerL=160;//outerW+imuBlockW;    //Length (X)
bottomHeight=3;
gesHeight=innerH+bottomHeight; //Boxheight (outside)

//Defines Bottom/Top
//height=innerH/2+imuHeight/2+bottomHeight;

//Motorblock dimensions
//motorX=0;
motorL=16;          //motor lenght
motorDia=7;         //motor diameter
mTol=0.3;           //print toleance

tolerance=0.3;


module fallingBoxBottom(){
    $fn=80;
    //Boxframe
        difference(){
            translate([0,0,innerH/2])cube([outerL,outerW,innerH+2*wall],center=true);
            translate([-outerL/2+wall,-outerW/2+wall,0])
                cube([outerL-wall*2,outerW-wall*2,innerH+2*wall]);
           translate([-outerL/2+wall,outerW/2-wall-1,innerH])cube([outerL-2*wall,wall+2,wall+1]);
           translate([0,0,innerH/2])testHoles();
           #switchDiff();
        }
    imuMount();

    //translate([0,0,-bottomHeight/2])boxEars(6,2,bottomHeight);

    /*
    //Sicks to mount the cap on the bottom
    stickSize=4;
    stickHeight=innerH;
    for (i = [0:180:360]){
         rotate([0,0,i])
            translate([-stickSize/2+outerL/2-wall,-stickSize/2+outerW/2-wall,stickHeight/2])
                cube([stickSize,stickSize,stickHeight],center=true);
     }
     mirror([0,1,0])for (i = [0:180:360]){
         rotate([0,0,i])
            translate([-stickSize/2+outerL/2-wall,-stickSize/2+outerW/2-wall,stickHeight/2])
                cube([stickSize,stickSize,stickHeight],center=true);
     }*/

    //only to show
    if(choice==4||choice==6)translate([26+9.5,0,0])rotate([0,0,180])engineBlock();
    if(choice==4||choice==6)translate([0,-7-26,0])rotate([0,0,90])engineBlock();
    if(choice==4||choice==6)translate([-50,0,30])color("orange")
        *text("Work in process!",size = 10,center=true);
    switches();
}

module fallingBoxCap(){
    $fn=80;
    //Boxframe
    translate([0,0,(height/2)-bottomHeight]){
        difference(){
            cube([outerL,outerW,height],center=true);
            translate([0,0,bottomHeight])
                cube([outerL-wall*2,outerW-wall*2,height],center=true);
        }
    }
    translate([0,0,-bottomHeight/2])boxEars(6,2,bottomHeight);
    imuMount();

    //only to show
    if(choice==4)translate([25,0,0])rotate([0,0,180])engineBlock();
    if(choice==4)translate([0,-20,0])rotate([0,0,90])engineBlock();
    if(choice==4)translate([-50,0,30])color("orange")
        text("Work in process!",size = 10,center=true);
}

module engineBlock(){
    $fn=100;
    blockHeiht=innerH/2;
    drillingDepth=blockHeiht-motorDia/2;    //for the M3-Holes

    difference(){
        union(){
            translate([-8,-12.5,0])cube([12,25,4]);
            translate([-4,-12.5,0])cube([4,25,innerH]);
        }
        for (i = [45:90:405]){
            translate([0,0,innerH/2])
                rotate([i,0,0])
                    translate([-2,0,10])rotate([0,90,0])cylinder(r=3/2, h=6,center=true);
        }
        translate([-2,0,innerH/2])rotate([0,90,0])cylinder(r=12/2 ,h=6 ,center=true);

    }
}

module imuMount(){
    imuW= 13;
    imuH= 5;
    imuWall =2;
    translate([0,0,innerH/2])
        difference(){
            cube([imuW+2*imuWall,imuH+2*imuWall,innerH],center=true);
            cube([imuW,imuH,innerH+1],center=true);
            translate([0,(imuH+imuWall)/2,0])cube([8,4,innerH+1], center=true);
        }
}
module testHoles(){
     for (i = [0:180:360]){
        rotate([0,0,i]){
            translate([outerL/2-3,outerW/2-3,0])rotate([0,90,-45])cylinder($fn=60,r=3/2, h=15, center=true);
            }
        }
     for (i = [90:180:450]){
        rotate([0,0,i]){
            translate([outerW/2-3,outerL/2-3,0])rotate([0,90,-45])cylinder($fn=60,r=3/2, h=15, center=true);
            }
     }
}


module boxEars(d,holed,h){
    r=d/2;
    hole=holed/2;
    for (i = [0:180:360]){
        rotate([0,0,i]){
            translate([outerL/2+1,outerW/2+1,0])rotate([0,0,45]){
                $fn=60;
                difference(){
                    union(){
                        cylinder(r=r, h=h, center=true);
                        translate([-r,0,0])cube([d,d,r],center=true);
                    }
                    cylinder(r=hole,h=h+1, center=true);
                }
            }
        }
    }
    mirror([0,1,0]){
            for (i = [0:180:360]){
                rotate([0,0,i]){
                    translate([outerL/2+1,outerW/2+1,0])rotate([0,0,45]){
                        $fn=60;
                        difference(){
                            union(){
                                cylinder(r=r, h=h, center=true);
                                translate([-r,0,0])cube([d,d,r],center=true);
                            }
                            cylinder(r=hole,h=h+1, center=true);
                        }
                    }
                }
            }
     }
}

module akku(){
  color("silver") {
    translate([-32/2,-22/2,0])cube(size=[32, 22, 6]);
  }
}
module teensy(){
  translate([152/2-8,124/2 -17.87/2 -4,0]){
    color("green") {
      translate([-9/2, -17.87/2, 0])cube(size=[9, 17.87, 35.56]);
    }
  }
}
module esc(){
  color("blue") {
    translate([-26/2,-18/2,0])cube(size=[26, 18, 5]);
  }
}

module switches(args) {
  translate([152/2-11,-45.5,0])cube(size=[11, 2, 10]);
  translate([152/2-11,-34.5,0])cube(size=[11, 2, 10]);
}

module switchDiff(args) {
    translate([(160-4)/2,0,0]){
      hull() {
        translate([0,-53.75-4.5,6/2+0.5])rotate([0,90,0])cylinder(r=6/2, h=10, center=true);//Schalter
        translate([0,-53.75+4.5,6/2+0.5])rotate([0,90,0])cylinder(r=6/2, h=10, center=true);
      }
      translate([0,-38.75,9/2+0.5])rotate([0,90,0])cylinder(r=5/2, h=10, center=true);//Taster
    }
}


if(choice==0||choice==4||choice==6)fallingBoxBottom();
if(choice==1)fallingBoxCap();
if(choice==2)engineBlock();
if(choice==3)imuMount();
if(choice==5)boxEars(6,2,3);

if(choice==6){
  fallingBoxBottom();
  translate([-71.5/2,0,0])rotate([0,0,90])akku();
  translate([-71.5/2,0,6])rotate([0,0,90])esc();
  translate([0,+57.5/2,0])akku();
  translate([0,+57.5/2,6])esc();
  translate([])teensy();
}

/* MotorTestProg
 *
 *  15.10.2015 Daniel Neutzler
 *
 */

#include"motorControl.h"
#include "brushlessControl.h"

#include <Servo.h>

#define eingabe       0   //0=100% 1500ms   1=Serielle Eingabe
#define motorType     0   //0-DC  1-BRUSHLESS

#define brushlessPin  9

#define motor1Input1  5
#define motor1Input2  6
#define motor1Enable  2
#define LEFT 0
#define RIGHT 1


motorControl motor1(motor1Input1, motor1Input2);    //Objekt DC Motor

brushlessControl *motor2;                           //Servo erstellen & Objekt Brushless erstellen
Servo             servoB;

void setup()
{
  pinMode(motor1Enable, OUTPUT);
  pinMode(motor1Input1, OUTPUT);
  pinMode(motor1Input2, OUTPUT);

  servoB.attach(brushlessPin);                   //Servo erstellen & Objekt Brushless erstellen
  motor2=new brushlessControl(servoB);
  Serial.begin(9600);
}


float motorPower = 0;
int   motorTime = 0;
int   help = 0;

void loop()
{

  if (motorType == 1) {
    motor2->power(motorPower);
    delay(1000);
  }
  else;
  
  while (1) {

    if (eingabe == 1) {
      Serial.println("MotorPower 0-100%: ");              //Eingabe Motorpower und Motorlaufzeit
      while (help == 0) {
        if (Serial.available() > 0) {
          //motorPower=Serial.read();
          motorPower = Serial.parseInt();
          help = 1;
        }
      }
      help = 0;
      Serial.println("Zeit Motor Dreht [ms]: ");
      while (help == 0) {
        if (Serial.available() > 0) {
          //motorTime=Serial.read();
          motorTime = Serial.parseInt();
          help = 1;
        }
      }
      help = 0;
      Serial.print(" Motorpower eingelesen: ");
      Serial.print(motorPower);
      Serial.print(" Zeit Motor Dreht eingelesen: ");
      Serial.println(motorTime);
    }
    else {
      motorTime = 1500;
      motorPower = 100;
      delay(2000);
    }


    if (motorType == 1) {
      motor2->power(motorPower);                           //Brushless Motorsteuerung
      delay(motorTime);
      motor2->power(0);
      delay(motorTime);
    }
    else {                                                // DC Motoransteuerung mit motorControl
      digitalWrite(motor1Enable, HIGH);                   //Enable Motor
      motor1.power(motorPower, LEFT);
      delay(motorTime);
      motor1.power(motorPower, RIGHT);
      delay(motorTime);
      digitalWrite(motor1Enable, LOW);                    //Disable Motor
    }

  }
}

/*
 * File:   brushlessControl.cpp
 * Author: Daniel
 *
 * Created on 5. November 2015, 09:01
 */

#include "brushlessControl.h"

brushlessControl::brushlessControl(Servo brushless) {
  this->brushless = brushless;
  powerBrushless=0;
}

void brushlessControl:: power(int i) {
  if (i > 100) {
    i = 100;
  }
  else;
  powerBrushless = (i / 100) * brushlessFullSpeed;

  if (powerBrushless < brushlessZero) {
    powerBrushless = brushlessZero;
  }
  else;
  brushless.write((int)powerBrushless);
}

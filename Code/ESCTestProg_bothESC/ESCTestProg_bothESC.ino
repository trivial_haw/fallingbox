#include <Servo.h>
#include<FastLED.h>
#define NUM_LEDS 4

#define DATA_PIN 5

CRGB leds[NUM_LEDS];

#define irpin 12

Servo SimonKESC;
Servo SimonKESC2;

int remote = 90; //starts with middle position
               // >= 95 -> counter clockwise
               // <= 85 -> clockwise

void setup(){
  FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);
  leds[0] = CRGB(255,0,0);
   FastLED.show();
   delay(1000);
   leds[0] = CRGB(0,0,0);
   FastLED.show();
SimonKESC.attach(9); //connect this PWM pin with the data cable of the ESC(usually white or yellow)
SimonKESC2.attach(8);
//motorESC.write(40); //if your ESC doesn`t have the SimonK firmware you can try this; remove the //signs
Serial.begin(115200);
pinMode(3, OUTPUT);      // sets the digital pin as output
SimonKESC.write(90);
SimonKESC2.write(90);
delay(2000);
}

void loop(){
//int poti = analogRead(potiPin);
//poti = map(poti, 0, 1023, 0, 179);

Serial.println(remote);
/*SimonKESC.write(100);
delay(3000);
SimonKESC.write(80);
delay(3000);
SimonKESC.write(90);/**/
/*
SimonKESC2.write(100);
delay(3000);
SimonKESC2.write(80);
delay(3000);
SimonKESC2.write(90);
/**/
if(Serial.available()){
  remote = Serial.parseInt();
}
SimonKESC.write(remote);
//SimonKESC.write(remote);
//while(!Serial.available()); 
//remote = Serial.parseInt();    // Parse an Integer from Serial
//
//if (remote > 90){
//  digitalWrite(3, HIGH);  //for oscilloscope
//} else {
//  digitalWrite(3,LOW);
//}
}

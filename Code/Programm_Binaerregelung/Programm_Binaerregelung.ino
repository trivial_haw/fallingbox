#include <Wire.h>
#include <L3G.h>
#include <LSM303.h>
#include <LPS.h>
#include <Servo.h>

#include "imu.h"
#include "motorControl.h"
#include "brushlessControl.h"

//DEBUG DEFINES*****************************************************
#define debugLevel 0    //0-nothing 1-SerialPrints
#define debugLed 14

//MOTORDEFINES******************************************************
#define dcMotorInput1  5    //DC Motor Pins
#define dcMotorInput2  2    //6
#define dcMotorEnable  6    //2
#define debugLed 14
#define LEFT 0
#define RIGHT 1

enum {DC, brushless};
#define motorType     DC     //0-DC Motor 1-Brushless Motor

#define brushlessPin  9
#define powerZero     86

brushlessControl *motor2;    //Servo erstellen & Objekt Brushless erstellen
Servo             servoB;

//CONTROL DEFINES***************************************************

#define powerM 100
#define setPoint 0        //Always ZERO because of circle of SVEN
#define desiredValue 45   //Angle we want to reach

//Ojects and Variables**********************************************

imu *imu1;
motorControl *dcMotor;

int    phiDiff = 0;
double currentValue = 0;
int    input = 0;

int help = 0;
unsigned long milliseconds = 0;

void setup() {

  Wire.begin();
  imu1 = new imu;
  imu1->initial();                                          //INITAL IMU

  pinMode(debugLed, OUTPUT);

  if (motorType == brushless) {
    servoB.attach(brushlessPin);                   //Servo erstellen & Objekt Brushless erstellen
    motor2 = new brushlessControl(servoB);
  };
  if (motorType == DC) {
    pinMode(dcMotorEnable, OUTPUT);                           //SET MOTOR PINS
    pinMode(dcMotorInput1, OUTPUT);
    pinMode(dcMotorInput2, OUTPUT);
    dcMotor = new motorControl(dcMotorInput1, dcMotorInput2); //DC Motor Object
    digitalWrite(dcMotorEnable, HIGH);                        //Enable DC-Motor
  };
  if (debugLevel == 1) {
    Serial.begin(9600);
  };
}

void loop() {
  //BEFORE LOOP
  milliseconds = millis();

  if (motorType == brushless) {
    motor2->power(powerZero);
    delay(1000);
  }

  while (1) {                            //LOOP

    imu1->actualize();                   //IMU current VALUES
    currentValue = imu1->getRollGyro();


    phiDiff = 360 - desiredValue;
    if (((int)currentValue + phiDiff) % 360 > 180) {
      input = (((int)currentValue + phiDiff) % 360) - 360;
    } else {
      input = ((int)currentValue + phiDiff) % 360;
    }

    if (debugLevel == 1) {
      Serial.print("current angle: ");
      Serial.print(currentValue);
      Serial.print("\t input: ");
      Serial.println(input);
    }

    if (input > 0) {

      if (motorType == DC) {
        dcMotor->power(powerM, RIGHT);//RIGHT
      }
      if (motorType == brushless) {
        motor2->power(powerM);
      }
      if (debugLevel == 1) {
        Serial.println("\t RIGHT");
      }

    } else {
      if (input < 0) {

        if (motorType == DC) {
          dcMotor->power(powerM, LEFT);//RIGHT
        }
        if (motorType == brushless) {
          motor2->power(powerM);
        }

        if (debugLevel == 1) {
          Serial.println("\t LEFT");
        }
      }
      else {

        if (motorType == DC) {
          dcMotor->zero();
        };
        if (motorType == brushless) {
          motor2->power(powerZero);
        };
      }
    }


    if (millis() - milliseconds > 10 * (abs(setPoint - input))) {
      help ^= true;
      digitalWrite(debugLed, help);
      milliseconds = millis();
    }

  }
}

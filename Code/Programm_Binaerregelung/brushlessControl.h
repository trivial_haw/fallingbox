/* 
 * File:   brushlessControl.h
 * Author: Daniel
 *
 * Created on 5. November 2015, 09:01
 */

#ifndef BRUSHLESSCONTROL_H
#define	BRUSHLESSCONTROL_H


#include <Servo.h>

#define brushlessZero 87
#define brushlessFullSpeed 255



class brushlessControl {
public:
    brushlessControl(Servo brushless);
    void power(int i);

private:
   Servo brushless;
   float powerBrushless;

};

#endif	/* BRUSHLESSCONTROL_H */


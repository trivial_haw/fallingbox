/* 
 * File:   MovingAverage.h
 * Author: Daniel
 *
 * Created on 25. Oktober 2015, 14:11
 */

#ifndef MOVINGAVERAGE_H
#define	MOVINGAVERAGE_H

#include <Arduino.h>

class MovingAverage {
public:
    MovingAverage(int n=20);
    
    void AddSample(float newSample);
    float getAverage();
    void reset();
    
private:

  float newSample;
  float *Samples;
  float erg;
  int count;
  int n;

  

};

#endif	/* MOVINGAVERAGE_H */


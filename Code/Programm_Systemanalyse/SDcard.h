#ifndef SDCARD_H
#define SDCARD_H

#include "SD.h"

class sdCard {
public:
  sdCard(char, char, char);
  ~sdCard();
  void write(float, float);
private:
  char chipSelectPin;
  String dataString;
  String fileName;
  unsigned int fileIndex;
  File dataFile;
  unsigned long startTime;
  unsigned long endTime;
  unsigned long operatingTime;
};

#endif /* SDCARD_H */


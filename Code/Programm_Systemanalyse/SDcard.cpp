#include "SDcard.h"

sdCard::sdCard(char chipSelectPin, char sdPin, char errorLEDPin) {
  //Im Konstruktor werden alle Variablen initialisiert, der Sd-Kartenslot aufgerufen,
  //eine Error-LED im Fehlerfall angeschaltet, ein einmaliger Dateiname erstellt und
  //der Filestream geöffnet
  fileIndex = 0;
  fileName = "";
  pinMode(sdPin, OUTPUT);
  while (!SD.begin(chipSelectPin)) {    //SD-Karte initialisieren
    pinMode(errorLEDPin,OUTPUT);        //Error-LED wenn SD nicht bereit
    digitalWrite(errorLEDPin, HIGH);
  }
  digitalWrite(errorLEDPin, LOW);
  while(1) {                           //solange die Datei nicht erstellt wurde
    fileName  = "DATA_";               //Logik für einmaligen Dateinamen
    fileName += fileIndex;
    fileName += ".txt";
    char charFileName[fileName.length() +1];
    fileName.toCharArray(charFileName, sizeof(charFileName));
    if (SD.exists(charFileName)) {    //wenn die Datei schon existiert
      fileIndex++;
    } 
    else {
      dataFile = SD.open(charFileName, FILE_WRITE);    //Datei wird auf SD-Karte erstellt
      break;                                           //Break um while-Schleife zu beenden
    }
  }
  dataString = "";                    //in dataString werden alle Werte gesammelt um sie auf einmal auf die SD-Karte zu schreiben
  dataString = "Time,Roll";  //Schreiben der Überschriften, um Werte den Sensoren zuzuordnen
  //dataFile.println(dataString);       //Ausgabe in den Filestream
  startTime = micros();
}

sdCard::~sdCard() {      //Destruktor
  dataFile.close();      //Schließen des Filestreams -> ZWINGEND erforderlich, sonst keine Daten auf SD!!!
  /*endTime = micros();
  operatingTime = endTime - startTime;
  
  Serial.print("Dauer der letzten Datenspeicherung: ");  //10.000 Daten ~6461ms (Arduino Uno)
  Serial.print(operatingTime);
  Serial.print(" us (");
  operatingTime = operatingTime / 1000;
  Serial.print(operatingTime);
  Serial.println(" ms)");/**/
}

void sdCard::write(float dataTime, float roll) {
  this->dataString = "";                  //Schreibe Sensordaten in den dataString
  this->dataString += String(dataTime);
  this->dataString += ",";
  this->dataString += String(roll);

  if (dataFile) {                         //wenn der Dateistrom noch geöffnet ist
    dataFile.println(dataString);         //Schreibe die Daten auf SD-Karte
  }
}

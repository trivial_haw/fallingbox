/* 
 * File:   brushlessControl.h
 * Author: Daniel
 *
 * Created on 5. November 2015, 09:01
 */
#include "Arduino.h"
#ifndef BRUSHLESSCONTROL_H
#define	BRUSHLESSCONTROL_H

#define brushlessZero 100
#define brushlessFullSpeed 255

class brushlessControl {
public:
    brushlessControl(int);
    void power(int);

private:
   int pwmPin;
   float powerBrushless;

};

#endif	/* BRUSHLESSCONTROL_H */


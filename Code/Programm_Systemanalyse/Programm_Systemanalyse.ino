#include <Wire.h>
#include <L3G.h>
#include <LPS.h>
#include <LSM303.h>
#include "imu.h"

#include <SPI.h>
#include "SDcard.h"
#include "SD.h"

#include "motorControl.h"
#include "brushlessControl.h"

#define debugLed 14

//DEFINES FOR MOTOR*************************************************
enum {DC, brushless};

#define motorType     DC     //0-DC Motor 1-Brushless Motor

#define dcMotorInput1  5     //DC Motor Pins
#define dcMotorInput2  2    //6
#define dcMotorEnable  6    //2

#define brushlessPin     6     //Brushless    

#define LEFT 0
#define RIGHT 1

#define motorPower 100
#define motorTime  2000

//DEFINES FOR SD CARD***********************************************
#define chipSelectPin  4 //Arduino Ethernet shield: pin 4
#define sdPin 10	 //Arduino Ethernet shield: pin 10
#define errorLEDPin 13   

sdCard* SDcard;
imu *imu1;

motorControl *dcMotor;
brushlessControl *brushlessMotor;

unsigned long seconds = 0;

void setup()
{
  Wire.begin();
  imu1 = new imu;
  imu1->initial();                 //INITAL IMU
  
  SDcard = new sdCard(chipSelectPin, sdPin, errorLEDPin);
  pinMode(debugLed, OUTPUT);

  if (motorType == DC) {
    pinMode(dcMotorEnable, OUTPUT);  //SET MOTOR PINS
    pinMode(dcMotorInput1, OUTPUT);
    pinMode(dcMotorInput2, OUTPUT);
    dcMotor = new motorControl(dcMotorInput1, dcMotorInput2); //DC Motor Object
  }
  if (motorType == brushless){
    brushlessMotor = new brushlessControl(brushlessPin);
  }
}

int milliseconds = millis();
bool help = false;

void loop() {

  if (motorType == brushless) {
    brushlessMotor->power(0);                                          //Enable Brushless Motor
  }
  if (motorType == DC) {
    digitalWrite(dcMotorEnable, HIGH);                          //Enable DC-Motor
  }
  delay(1000);

  while (1) {
    
    seconds = millis();
    if (motorType == brushless) {
      brushlessMotor->power(motorPower);
    }
    if (motorType == DC) {
      dcMotor->power(motorPower, LEFT);    
    }
    
    while ((millis() - seconds) <= motorTime) {
      imu1->actualize();
      SDcard->write(imu1->getProcessTime(), imu1->getGyroX());    //AUSGABE SERIAL UND SD KARTE
      if (millis() - milliseconds > 500) {
        help ^= true;
        digitalWrite(debugLed, help);
        milliseconds = millis();
      }
    }
                                                                    //MOTOR AUS
    seconds = millis();
    if (motorType == brushless) {
      brushlessMotor->power(0);
    }
    if (motorType == DC) {
      dcMotor->zero();
    }
    
    while ((millis() - seconds) <= motorTime) {
      imu1->actualize();
      SDcard->write(imu1->getProcessTime(), imu1->getGyroX());    //AUSGABE SERIAL UND SD KARTE
      if (millis() - milliseconds > 100) {
        help ^= true;
        digitalWrite(debugLed, help);
        milliseconds = millis();
      }
    }
    
    delete SDcard;
    while (1) {
        digitalWrite(debugLed, HIGH);
    }
    
  }
}

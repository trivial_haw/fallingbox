/*
 * File:   brushlessControl.cpp
 * Author: Daniel
 *
 * Created on 5. November 2015, 09:01
 */

#include "brushlessControl.h"

brushlessControl::brushlessControl(int pin) {
  this->pwmPin = pin;
  powerBrushless=0;
}

void brushlessControl:: power(int i) {
  if (i > 100) {
    i = 100;
  }
  powerBrushless = (i / 100) * brushlessFullSpeed;

  if (powerBrushless < brushlessZero) {
    powerBrushless = brushlessZero;
  }
  analogWrite(pwmPin, powerBrushless);
}

#include <Wire.h>
#include <L3G.h>
#include <LSM303.h>
#include <LPS.h>
#include <Servo.h>
#include "LEDManagement.h"
#include "defines.h"
#include "box.h"
#include "voltage.h"
#include "imu.h"

enum mode {Idle,Auto, Debug, AkkuSafe};
short mode = Idle;  
short oldMode = Idle;

bool isSerialOn = true;

Box *fallingBox;
voltage *akku;
imu* imu1;
LEDManagement* led;

unsigned long startTime = 0;
unsigned long helpTime = 0;

void setup() {
  Wire.begin();
  delay(20);
  imu1=new imu();
  imu1->initial();
  fallingBox = new Box(imu1);
  akku = new voltage(AKKUPOWERPIN);
  led = new LEDManagement(LEDSTRIPE);

  pinMode(DEBUGLED, OUTPUT);
  pinMode(DEBUGPIN ,INPUT);
  pinMode(AUTOMODEPIN ,INPUT);
  pinMode(PIN3 ,INPUT);
  pinMode(PIN4 ,INPUT);

  pinMode(13,OUTPUT);
  digitalWrite(13, HIGH);
  Serial.begin(9600);
  
  startTime =millis();
}

void loop() {
  helpTime = millis();
  if(helpTime-startTime > HEARTBEATTIME){
    led->setColor(HEARTBEAT, BLUE);
    startTime = millis();
  }
  imu1->update();      //aktualisiere IMU

  if(digitalRead(AUTOMODEPIN) && imu1->falling()){
    mode = Auto;
    led->allLEDsOff();
  }
 if(digitalRead(AUTOMODEPIN) && !imu1->falling()){
   mode = Idle;
   led->allLEDsOff();
  }
  if(digitalRead(DEBUGPIN)){
    mode = Debug;
    led->allLEDsOff();
  }

  if(!akku->isVoltageOk() && !imu1->falling()){
    mode= AkkuSafe;
    led->allLEDsOff();
  }

  if((mode == Debug) && !isSerialOn){
    Serial.begin(9600);
    delay(20);
    isSerialOn = true;
  }
  if((mode == !Debug) && isSerialOn){
    Serial.end();
    isSerialOn = false;
  }



  switch (mode) {
      case Idle:
        led->setColor(STATUS, GREEN);
        fallingBox->setEnginesToZero();
        break;

      case Auto:
       led->setColor(STATUS, BLUE);
        fallingBox->binaryControl(false);
        break;

      case Debug:
        led->setColor(DEBUG, RED);
        Serial.println("Debug");
        fallingBox->binaryControl(true);
        break;

      case AkkuSafe:
        led->setColor(BATTERY, RED);
        break;

      default:
        break;
  }
}

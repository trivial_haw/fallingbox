//--------------Pin-Defines --------------
#define ROLLPIN      8
#define PITCHPIN     9
#define DEBUGLED     14
#define LEDSTRIPE    5
#define AKKUPOWERPIN 17
//Modeswitchpins
#define AUTOMODEPIN 23 //Schitch [1]
#define DEBUGPIN 22    //Switch [2]
#define PIN3     21   //Switch [3]
#define PIN4     20   //Switch [4]
//----------------------------------------

//BeautyStuff
#define HEARTBEATTIME 200    //5Hz

//Critical Voltage
#define MINVOLTAGE 6.1  //[V]

//BrushlessDefines
#define CLOCKWISE 173         //Brushless Max Speed  Clockwise
#define COUNTERCLOCKWISE 3    //Brushless Max Speed CounterClockwise
#define ZEROSPEED 90          //Brushless Zero Speed

//BinarycontrollerDefines
#define BINARY_CLOCKWISE  160
#define BINARY_COUNTERCLOCKWISE 20
#define DESIREDANGLE 0

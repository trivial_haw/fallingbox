#ifndef VOLTAGE_H
#define	VOLTAGE_H

/* Diese Klasse wird benutzt um die Spannung zu messen. Sie muss individuell an den Analogeingang angepasst werden.
   Bei der Falling Box ist der Spannungsteiler 10KOhm zu 5KOhm am Analogeingang. Ein Objekt mit dem dementsprechenen Pin übergeben mit "getVoltage" kann der Wert ausgegeben werden.
   Mit "isVolatgeOk" kann überprüft werden ob wir oberhalb der Entladeschwelle sind.*/


#include "defines.h"
#include "MovingAverage.h"

class voltage {
public:
    voltage(int pin);
    float getVoltage();
    bool isVoltageOk();
private:
  MovingAverage *average;
  void updateVoltage();
  int pin;
  float value;
};

#endif	/* VOLTAGE_H */


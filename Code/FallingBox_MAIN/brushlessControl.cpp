#include "brushlessControl.h"

brushlessControl::brushlessControl(int pin) {
  this->pwmPin = pin;
  pinMode(pwmPin, OUTPUT);
  
  this->SimonKESC = new Servo();
  this->SimonKESC->attach(pwmPin);
  this->SimonKESC->write(ZEROSPEED);

  //Give the brushless some time to initialize
  delay(2000);  
}

void brushlessControl::rotate(bool debug, int i) {
  if(i < COUNTERCLOCKWISE){
    i = COUNTERCLOCKWISE;
  }
  if(i > CLOCKWISE){
    i = CLOCKWISE;
  }
  SimonKESC->write(i);
  if(debug){
    Serial.print("EscValue:  ");
    Serial.println(i);
  }
}

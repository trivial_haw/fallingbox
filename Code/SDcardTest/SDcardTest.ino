#include "SDcard.h"
#include "SD.h"
#include <SPI.h>
#define chipSelectPin 4   //Arduino Ethernet shield: pin 4
#define sdPin 10	        //Arduino Ethernet shield: pin 10
#define errorLEDPin 2     // Orangene LED auf dem Teensy

sdCard* SDcard;

int j;

void setup() {
  Serial.begin(9600);
  SDcard = new sdCard(chipSelectPin,sdPin,errorLEDPin);
  j=0;
}

void loop() {
  
  if(j==0) {
    for (int i=0;i<1000;i++) {
      SDcard->write(3.14,i);
    }
    delete SDcard;
    j++;
  }
}


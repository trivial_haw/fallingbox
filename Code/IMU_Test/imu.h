#ifndef IMU_H
#define	IMU_H

/*
 Die IMU Klasse ist daf�r geschrieben das s�mmtliche Sensoren einfach ausgewertet werden k�nnen.
 
 In der Klasse "actualize" und "inital" kann man einzelne Sensorenbaugruppen auskommentieren, die man aktuell
 nicht braucht.
 
 */
#include <L3G.h>
#include <LSM303.h>
#include <LPS.h>
#include "MovingAverage.h"

class imu {
public:
    imu();
    void initial();
    void update();

    double getGyroX();
    double getGyroY();
    double getGyroZ();
    
    float getRollGyro();
    float getPitchGyro();
    float getHeadingGyro();

    float getAccX();
    float getAccY();
    float getAccZ();
    
    float getPitch();
    float getRoll();
    
    float getMagX();
    float getMagY();
    float getMagZ();
    
    float getHeading();

    float getAltitude();
    float getPressure();
    float getTemperature();

    bool falling();
    
private:

#define fall 0.6

    void updateGyro();
    void updateAltimeter();
    void updateCompass();

    void initialGyro();
    void initialAltimeter();
    void initialCompass();
    

L3G gyro;
LPS altimeter;
LSM303 compass;

MovingAverage *gyroXAverage;
MovingAverage *gyroYAverage;
MovingAverage *gyroZAverage;


MovingAverage *rollAverage;
MovingAverage *pitchAverage;
MovingAverage *headingAverage;


double gyroX;
double gyroY;
double gyroZ;

double  newGyroX;
double  newGyroY;
double  newGyroZ;

double gyroXOffset;
double gyroYOffset;
double gyroZOffset;
float angleX;
float angleY;
float angleZ;


unsigned long oldTime;
unsigned long newTime;
unsigned long deltaT;

float pressure;
float QNH;
float altitude;
float temperature;

float aX;
float aY;
float aZ;
float aXOffset;
float aYOffset;
float aZOffset;
float pitch;
float roll;
float newPitch;
float newRoll;

float mX;
float mY;
float mZ;
float heading;
float newHeading;

};

#endif	/* IMU_H */


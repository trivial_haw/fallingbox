
#include <Wire.h>
#include <L3G.h>
#include <LPS.h>
#include <LSM303.h>
#include "imu.h"

imu *imu1;

void setup() 
{ 
Wire.begin();

imu1= new imu;

imu1->initial();
Serial.begin(9600);
} 

int milliseconds = millis();
bool help = false;

void loop() 
{

imu1->actualize();
imu1->falling();

delay(50);
/*
Serial.print(" deg/s in X:" );           //Drehrate
Serial.print(imu1->getGyroX());
Serial.print(" deg/s in Y:" );
Serial.print(imu1->getGyroY());
Serial.print(" deg/s in Z: ");
Serial.println(imu1->getGyroZ());
/**/


Serial.print("  pitchGyro:  ");             //Winkel aus Gyro
Serial.print(imu1->getPitchGyro());
Serial.print("  rollGyro:  ");
Serial.print(imu1->getRollGyro());
Serial.print("  headingGyro:  ");
Serial.print(imu1->getHeadingGyro());
/**/

/*
Serial.print(" P in mBar: ");               //Höhe Druck Temperatur
Serial.print(imu1->getPressure());
Serial.print("hoehe in m:");
Serial.print(imu1->getAltitude());
Serial.print(" Tem in degC: ");
Serial.println(imu1->getTemperature());
/**/

/*
Serial.print("pitch:  ");                   // Winkel vom Gyro vs. Winkel vom Accelerometer
Serial.print(imu1->getPitch());
Serial.print("  pitchGyro:  ");
Serial.print(imu1->getPitchGyro());
Serial.print("  roll:  ");
Serial.print(imu1->getRoll());
Serial.print("  rollGyro:  ");
Serial.print(imu1->getRollGyro());
Serial.print("  heading:  ");
Serial.print(imu1->getHeading());
Serial.print("  headingGyro:  ");
Serial.println(imu1->getHeadingGyro());
/**/

Serial.print("  pitch:  ");                   //Winkel vom Accelerometer
Serial.print(imu1->getPitch());
Serial.print("  roll:  ");
Serial.print(imu1->getRoll());
Serial.print("  heading:  ");
Serial.print(imu1->getHeading());
/**/

/*
Serial.print("!ANG:");                      //PYTHON SKRIPT KOMMUNIKATION  Baudrate: 115200  !!!!
Serial.print(imu1->getRoll());
Serial.print(",");
Serial.print(imu1->getPitch());
Serial.print(",");
Serial.println(imu1->getHeading());
/**/
}
 

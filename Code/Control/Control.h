#ifndef Control_H
#define	Control_H
#include "Arduino.h"

class Control {
public:
  Control();    //ACHSE MITGEBEN
  virtual ~Control();
  float pid(float, float);

private:
  eepromGui *parameters;
  float esum;
  float ealt;   //Vergangenheit
  float w;      //Führungsgröße (Sollwert)
  float e;      //Regelabweichung (Istwert)
  float Kp;     //Proportionalbeiwert
  float Ki;     //Integrierbeiwert
  float Kd;     //Differenzierbeiwert
};

#endif

#include "Control.h"

Control::Control(){
  this->esum=0;
  this->ealt=0;
  this->e=0;
  parameters= new eepromGui();
  this->w=parameters.eprmRead(0);
  this->Kp= parameters.eprmRead(4);
  this->Kd= parameters.eprmRead(8);
  this->Ki= parameters.eprmRead(12);
  delete parameters;
}

float Control::pid(float x, float Ta){
  e=w-x;              //Regeldifferenz
  esum = esum + e;
  y = Kp * e + Ki * Ta * esum + Kd * (e – ealt)/Ta;
  ealt = e;
  return y;
}

/*Einfacher Test um den Sammelschalter 1-4 zu testen*/

void setup() {
  pinMode(23, INPUT);  
  pinMode(22, INPUT);
  pinMode(21, INPUT);
pinMode(20, INPUT); 
pinMode(17, INPUT);  
}

void loop() {
  Serial.print("S1 = ");
  Serial.print(digitalRead(23));
  Serial.print(" S2 = ");
  Serial.print(digitalRead(22));
  Serial.print(" S3 = ");
  Serial.print(digitalRead(21));
  Serial.print(" S4 = ");
  Serial.print(digitalRead(20));
  Serial.print(" Akku = ");
  Serial.println(analogRead(17));
}

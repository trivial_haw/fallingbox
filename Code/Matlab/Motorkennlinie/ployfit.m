data = csvread('Motorkennlinien_both.csv');
pwm = data(:,1);
speed = data(:,2);

plot(pwm, speed,'.');
hold on
for(i=16)
    p = polyfit(pwm,speed,i);
    p1= polyval(p,pwm);
    plot(pwm, p1);
    legend('i');
end
hold off
legend('Testdaten','Polynom', i,'Location','southeast');
legend;
figure;
plot(pwm,p1);
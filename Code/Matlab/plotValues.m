filename = 'DCmotor/roll/drehrate/2015-11-05_009.TXT';
M = csvread(filename);
% Save time information in single array (Row one of the csv file) 
time = M(:,1);
deltaTime = M(:,1) - M(1,1);
deltaTimeSec = deltaTime / 1e3;
% Save roll information in single array (Row two of the csv file) 
roll = -1 * M(:,2);
% Save pitch information in single array (Row three of the csv file) 
%pitch = M(:,3);
% Save heading information in single array (Row four of the csv file) 
%heading = M(:,4);

%% integrate gyro
rollAngle = cumtrapz(deltaTimeSec,roll);

%% analyse IT1
m = (rollAngle(550) - rollAngle(325))/(deltaTime(550) - deltaTime(325));
b = rollAngle(325) - m * deltaTime(325);
rollTangente = m * deltaTime + b;
rollTangenteZero = m * deltaTime;
it1MilliSec = -b / m;
ki = m * 1000
t1 = it1MilliSec / 1000

%% Plots
figure;
plot(deltaTime,roll);
title('Roll');
grid on;
grid minor;
xlabel('time [ms]');
ylabel('rotationRate [°/s]');

figure;
plot(deltaTime,rollAngle);
hold on;
plot(deltaTime,rollTangente);
plot(deltaTime,rollTangenteZero);
title('Roll');
grid on;
grid minor;
xlabel('time [ms]');
ylabel('rotation [°]');

%{
% Plot pitch values
figure;
plot(time,pitch);
title('Pitch');
grid on;
grid minor;
xlabel('time [\mus]');
ylabel('rotation [�]');

% Plot heading values
figure;
plot(time,heading);
title('Heading');
grid on;
grid minor;
xlabel('time [\mus]');
ylabel('rotation [�]');
%}


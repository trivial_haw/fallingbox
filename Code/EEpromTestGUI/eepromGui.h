#ifndef eepromGui_H
#define	eepromGui_H
#include "Arduino.h"
#include "EEPROM.h"

class eepromGui {
public:
  eepromGui();
  void eprmWrite(float,float,float);
  float eprmRead(int);
  void eprmClear();
  char startFrame;
private:
  int addr;
  char data[sizeof(float)];//4Bytes
};

#endif	/* eepromGui_H */



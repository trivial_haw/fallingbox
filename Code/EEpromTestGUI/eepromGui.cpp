#include "eepromGui.h"

eepromGui::eepromGui(){
  this->addr = 0;
}

void eepromGui::eprmWrite(float val1, float val2, float val3){  
  addr=0;
  memcpy(data, &val1, sizeof val1);    // send data
  for(int i=0; i<4; i++){
    EEPROM.write(addr+i, data[i]); 
  }
  addr=4;
  memcpy(data, &val2, sizeof val2);    // send data
  for(int i=0; i<4; i++){
    EEPROM.write(addr+i, data[i]); 
  }
  addr=8;
  memcpy(data, &val3, sizeof val3);    // send data
  for(int i=0; i<4; i++){
    EEPROM.write(addr+i, data[i]); 
  }
}

float eepromGui::eprmRead(int adr){ 
  float outputFloat=0;
  for( int i=0; i<4; i++){
    data[i]=EEPROM.read(adr+i); 
  }
  memcpy(&outputFloat, data, sizeof outputFloat);// receive data   
  return outputFloat;    
}

void eepromGui::eprmClear(){
  for (int i = 0; i < 512; i++)
    EEPROM.write(i, 0);
}

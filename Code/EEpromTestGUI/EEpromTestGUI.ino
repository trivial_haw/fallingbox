#include <EEPROM.h>
#include "eepromGui.h"
//This a Float-Based EEPROM-Software each Value 4BYtes -> 4 EEPROM adresses
int addr=0;
float output=0;
union float2bytes { float f; byte b[sizeof(float)]; };
float2bytes p,i,d;

eepromGui* parameters;
char at=0;  

void setup()
{
  Serial.begin(115200);
  parameters=new eepromGui();
  Serial.flush();
  pinMode(13,OUTPUT);
  digitalWrite(13,LOW);
}

void loop(){
  
  if(Serial.available() && at != '@'){
    at=Serial.read();
  }
  if((Serial.available() >= 16) && (at == '@')){
    for(int a=0; a<4; a++){
      p.b[a]= Serial.read();
    }
    for(int a=0; a<4; a++){
      i.b[a]= Serial.read();
    }
    for(int a=0; a<4; a++){
      d.b[a]= Serial.read();
    }
    at=0;
    parameters->eprmWrite(p.f, i.f, d.f);
  } 
  delay(15);
  p.f= parameters->eprmRead(0);
  i.f= parameters->eprmRead(4);
  d.f= parameters->eprmRead(8);
  if(p.f==4){
    digitalWrite(13,HIGH);
  }else{
    digitalWrite(13,LOW);
  }
  delay(15);
  Serial.print('@');
  Serial.write(p.b, 4);
  Serial.write(i.b, 4);
  Serial.write(d.b, 4);
  delay(10);
}




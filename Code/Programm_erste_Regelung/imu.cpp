/*
 * File:   imu.cpp
 * Author: Daniel
 *
 * Created on 10. Oktober 2015, 17:23
 */

#include "imu.h"

imu::imu() {

  gyroX = 0;
  gyroY = 0;
  gyroZ = 0;
  gyroXOffset = 0;
  gyroYOffset = 0;
  gyroZOffset = 0;
  angleX = 0;
  angleY = 0;
  angleZ = 0;
  oldTime = 0;
  newTime = 0;
  deltaT = 0;

  pressure = 0;
  QNH = 0;
  altitude = 0;
  temperature = 0;

  aX = 0;
  aY = 0;
  aZ = 0;
  aXOffset = 0;
  aYOffset = 0;
  aZOffset = 0;
  pitch = 0;
  roll = 0;
  newPitch=0;
  newRoll=0;

  mX = 0;
  mY = 0;
  mZ = 0;
  heading=0;
  newHeading=0;


  gyroXAverage=new MovingAverage(5);
  gyroYAverage=new MovingAverage(5);
  gyroZAverage=new MovingAverage(5);

  rollAverage= new MovingAverage(1);//10);     //Festlegen der Anzahl der Mittelwerte
  pitchAverage= new MovingAverage(1);
  headingAverage=new MovingAverage(1);
}

void imu::initial() {

  initialGyro();
  //initialAltimeter();                   //brauchen wir aktuell nicht
  initialCompass();
}

void imu::actualize() {

  actualizeGyro();
  //actualizeAltimeter();                 //brauchen wir aktuell nicht
  actualizeCompass();

  rollAverage->AddSample(roll);
  pitchAverage->AddSample(pitch);
  headingAverage->AddSample(heading);

  //MOVING AVERAGE !!!
  newRoll=rollAverage->getAverage();
  newPitch=pitchAverage->getAverage();
  newHeading=headingAverage->getAverage();
}

void imu::initialGyro() {
  gyro.init();                                        //Initialisiert Gyro
  gyro.enableDefault();
  gyro.writeReg(0x20, 0xFF);                          //CTRL1=0x20 ODR 800Hz  CutOff 100Hz  =0xFF
  gyro.writeReg(0x21, 0x20);                          //CTRL2=0x21  HIGHPASS FILTER 30Hz=0x21
  gyro.writeReg(0x23, 0x20);                          //CTRL4=0x10  0x00->250dps 8.75   0x10->500dps 17.50  0x20->2000dps 70

/*
  for (int i = 0; i < 1000; i++) {                     //automatische Offsetberechung
    gyro.read();
    gyroXOffset = gyroXOffset + (gyro.g.x) * 70;
    gyroYOffset = gyroYOffset + (gyro.g.y) * 70;
    gyroZOffset = gyroZOffset + (gyro.g.z) * 70;
  }
  gyroXOffset = gyroXOffset / 1000;
  gyroYOffset = gyroYOffset / 1000;
  gyroZOffset = gyroZOffset / 1000;
/**/
gyroXOffset=-2242.87;
gyroYOffset=2239.58;
gyroZOffset=848.47;

/*
Serial.print(" XOffset:  ");
Serial.print(gyroXOffset);
Serial.print(" YOffset:  ");
Serial.print( gyroYOffset);
Serial.print(" ZOffset:  ");
Serial.println(gyroZOffset);
/**/  
}

void imu::initialAltimeter() {

  altimeter.init();                               //Initialisiert Altimeter
  altimeter.enableDefault();
  altimeter.writeReg(0x10, 0x79);                 //Auflösung der Temperatur 128  Durchschnittsbildung Druck 256=0x78   RES_CONF=0x10

  QNH = altimeter.readPressureMillibars();        //Druck am Grund ermitteln
}

void imu::initialCompass() {

  compass.init();                                 //Initialisiert Compass
  compass.enableDefault();

  compass.writeReg(0x20, 0x97);                          //CRTL1=0x20  ODR 800Hz  alle Achsen Beschleunigung enable=0x57
  compass.writeReg(0x21, 0xC0);                          //CRTL2=0x21  AntiAliasFilter für Beschleunigung 194Hz =0x40    362Hz=0x80   773Hz=0x00  50Hz=0xC0

  compass.writeReg(0x24, 0x74);                          //CTRL5=0x24  ODR 100Hz  für Magnetometer  HighResolution, TempDisabled =0x74

  /*
  compass.m_min = (LSM303::vector<int16_t>) {   //callibriere Kompass default Values
    -32767, -32767, -32767
  };
  compass.m_max = (LSM303::vector<int16_t>) {
    +32767, +32767, +32767
  };
  /**/

compass.m_min = (LSM303::vector<int16_t>) {     //callibriere Kompass Werte aus FSR Raum gemessen
    -2977, -2355, -2249
  };
  compass.m_max = (LSM303::vector<int16_t>) {
    +2926, +2835, +3453
  };


  for (int i = 0; i < 100; i++) {
    compass.read();
    aXOffset = aXOffset + compass.a.x * 0.061;       //Offset der Beschleunigungen
    aYOffset = aYOffset + compass.a.y * 0.061;
    aZOffset = aZOffset + compass.a.z * 0.061;
  }

  aXOffset = aXOffset / 100;
  aYOffset = aYOffset / 100;
  aZOffset = aZOffset / 100;

  if (aXOffset > 500) {
    aXOffset = aXOffset - 1000;                      //Automatische Gravitationsausrichtung "welche Achse liegt in g Vector Richtung"
  }
  if (aYOffset > 500) {
    aYOffset = aYOffset - 1000;
  }
  if (aZOffset > 500) {
    aZOffset = aZOffset - 1000;
  }
  if (aXOffset < -500) {
    aXOffset = aXOffset + 1000;
  }
  if (aYOffset < -500) {
    aYOffset = aYOffset + 1000;
  }
  if (aZOffset < -500) {
    aZOffset = aZOffset + 1000;
  }
  heading = compass.heading();
}

void imu::actualizeGyro() {

  gyro.read();                                         // Drehraten abfragen
  gyroX =((gyro.g.x) * 70 - gyroXOffset) / 1000;    // Rohwerte der Drehrate!!   bei Sensitivitätseinstellung +-245dps   sind es 8.75mdps   Grad/millisekunde
  gyroY =((gyro.g.y) * 70 - gyroYOffset) / 1000;    //in Grad pro Sekunde
  gyroZ =((gyro.g.z) * 70 - gyroZOffset) / 1000;

    gyroXAverage->AddSample(gyroX);
    gyroYAverage->AddSample(gyroY);
    gyroZAverage->AddSample(gyroZ);

  newGyroX=gyroXAverage->getAverage();
  newGyroY=gyroYAverage->getAverage();
  newGyroZ=gyroZAverage->getAverage();



  if (oldTime <= newTime) {
    newTime = millis();
  }
  if (oldTime != 0) {
    deltaT = newTime - oldTime;
  }

  angleX = angleX + newGyroX * deltaT / 1000;         //Integration und Aufsummation der Drehraten zu einem Winkel in Grad
  angleY = angleY + newGyroY * deltaT / 1000;         //1000 millisecunden zu sekunden
  angleZ = angleZ + newGyroZ * deltaT / 1000;
  ;
  oldTime = newTime;
  
}

void imu::actualizeAltimeter() {                                                                            //DRUCKGENAUIGKEIT +-0.2 bis +-0.1 mbar => max +-1.7m

  pressure = altimeter.readPressureMillibars();                                                             // aktueller Druck in mBar
  //altitude = (1 - pow(pressure / QNH, 0.190295)) * (((altimeter.readTemperatureC()) + 273.15) / 0.0065);  //Höhe über Grund in m mit zurechnung der Temperatur/Temperaturgradient
  altitude = altimeter.pressureToAltitudeMeters(pressure, QNH);                                             // Höhe über GRUND in m mit der internen Formel
  temperature = altimeter.readTemperatureC();                                                               // aktuelle Temperatur in °C
}

void imu::actualizeCompass() {

  compass.read();
  aX = (compass.a.x * 0.061 - aXOffset) / 1000;                                                             //   0.061 mg/dsp -> mg
  aY = (compass.a.y * 0.061 - aYOffset) / 1000;                                                             //
  aZ = (compass.a.z * 0.061 - aZOffset) / 1000;

  roll = 180 + (atan2(-aY, aZ) * 180) / PI;
  pitch = 180 + (atan2(-aX, aZ) * 180) / PI;

  mX = compass.m.x * 0.160;
  mY = compass.m.y * 0.160;
  mZ = compass.m.z * 0.160;
  heading = compass.heading();
}


double imu::getGyroX() {
  return gyroX;
}
double imu::getGyroY() {
  return gyroY;
}
double imu::getGyroZ() {
  return gyroZ;
}
float imu::getRollGyro() {
  return angleX;
}
float imu::getPitchGyro() {
  return angleY;
}
float imu::getHeadingGyro() {
  return angleZ;
}

float imu::getAccX() {
  return aX;
}

float imu::getAccY() {
  return aY;
}

float imu::getAccZ() {
  return aZ;
}

float imu::getPitch() {
  return newPitch;
}
float imu::getRoll() {
  return newRoll;
}

float imu::getMagX() {
  return mX;
}
float imu::getMagY() {
  return mY;
}
float imu::getMagZ() {
  return mZ;
}

float imu::getAltitude() {
  return altitude;
}

float imu::getPressure() {
  return pressure;
}

float imu::getTemperature() {
  return temperature;
}

float imu::getHeading() {
  return newHeading;
}


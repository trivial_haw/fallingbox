/* 
 * File:   imu.h
 * Author: Daniel
 *
 * Created on 10. Oktober 2015, 17:23
 */

#ifndef IMU_H
#define	IMU_H

#include <L3G.h>
#include <LSM303.h>
#include <LPS.h>
#include "MovingAverage.h"

class imu {
public:
    imu();
    void initial();
    void actualize();

    double getGyroX();
    double getGyroY();
    double getGyroZ();
    
    float getRollGyro();
    float getPitchGyro();
    float getHeadingGyro();

    float getAccX();
    float getAccY();
    float getAccZ();
    
    float getPitch();
    float getRoll();
    
    float getMagX();
    float getMagY();
    float getMagZ();
    
    float getHeading();

    float getAltitude();
    float getPressure();
    float getTemperature();
private:

    void actualizeGyro();
    void actualizeAltimeter();
    void actualizeCompass();

    void initialGyro();
    void initialAltimeter();
    void initialCompass();
    

L3G gyro;
LPS altimeter;
LSM303 compass;

MovingAverage *gyroXAverage;
MovingAverage *gyroYAverage;
MovingAverage *gyroZAverage;


MovingAverage *rollAverage;
MovingAverage *pitchAverage;
MovingAverage *headingAverage;


double gyroX;
double gyroY;
double gyroZ;

double  newGyroX;
double  newGyroY;
double  newGyroZ;

double gyroXOffset;
double gyroYOffset;
double gyroZOffset;
float angleX;
float angleY;
float angleZ;


unsigned long oldTime;
unsigned long newTime;
unsigned long deltaT;

float pressure;
float QNH;
float altitude;
float temperature;

float aX;
float aY;
float aZ;
float aXOffset;
float aYOffset;
float aZOffset;
float pitch;
float roll;
float newPitch;
float newRoll;

float mX;
float mY;
float mZ;
float heading;
float newHeading;

};

#endif	/* IMU_H */


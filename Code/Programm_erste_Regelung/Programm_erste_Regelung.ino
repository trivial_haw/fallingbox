#include <Wire.h>
#include <L3G.h>
#include <LSM303.h>
#include <LPS.h>
#include "imu.h"
#include "PID_v1.h"
#include "motorControl.h"

//DEBUG DEFINES*****************************************************
#define debugLevel 0   //0-nothing 1-SerialPrints
#define debugLed 14

//DEFINES FOR MOTOR********************************************************************************************************
#define dcMotorInput1  5    //DC Motor Pins
#define dcMotorInput2  2    //6
#define dcMotorEnable  6    //2
#define debugLed 14
#define LEFT 0
#define RIGHT 1


//DEFINES FOR CONTROLLER***************************************************************************************************
double Setpoint = 0; //ALWAYS ZERO because of Svens Circle
double Input, Output;
//Specify the links and initial tuning parameters
double Kp = 0.491924, Ki = 0.014611624, Kd = 0.176273;
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT); //DIRECT means the output will increase when error is positive.

int phiDiff = 0;
double desiredValue = 45;   //Angle we want to reach!!
double imuData = 0;

//Objects and Values*******************************************************************************************************

imu *imu1;
motorControl *dcMotor;

double roll;
int milliseconds = millis();
bool help = false;

void setup() {
  Wire.begin();
  imu1 = new imu;
  imu1->initial();                    //INITAL IMU

  pinMode(debugLed, OUTPUT);
  pinMode(dcMotorEnable, OUTPUT);     //SET MOTOR PINS
  pinMode(dcMotorInput1, OUTPUT);
  pinMode(dcMotorInput2, OUTPUT);
  digitalWrite(dcMotorEnable, HIGH);  //Enable DC-Motor
  dcMotor = new motorControl(dcMotorInput1, dcMotorInput2); //DC Motor Object

  myPID.SetOutputLimits(-100.0, 100.0);
  myPID.SetMode(AUTOMATIC);           //turn the PID on

  if (debugLevel == 1) {
    Serial.begin(115200);
  }
}

void loop() {
  imu1->actualize();
  imuData = imu1->getRollGyro();

  phiDiff = 360 - desiredValue;
  if (((int)imuData + phiDiff) % 360 > 180) {
    Input = (((int)imuData + phiDiff) % 360) - 360;
  } else {
    Input = ((int)imuData + phiDiff) % 360;
  }

  myPID.Compute();

  if (debugLevel == 1) {
    Serial.print("input angle:");
    Serial.print(Input);
    Serial.print("\t motorPower:");
    Serial.print(Output);
  }

  if (Output < 0) {
    dcMotor->power(abs(Output), RIGHT);//RIGHT
    if (debugLevel == 1) {
      Serial.println("\t RIGHT");
    }
  } else {
    if (Output > 0) {
      dcMotor->power(abs(Output),LEFT);  //LEFT
      if (debugLevel == 1) {
        Serial.println("\t LEFT");
      }
    } else {
      dcMotor->zero();
    }
  }
  if (millis() - milliseconds > 10 * (abs(Setpoint - Input))) {
    help ^= true;
    digitalWrite(debugLed, help);
    milliseconds = millis();
  }
}

#include <PWMServo.h>

#define irpin 12

PWMServo SimonKESC;

int remote = 90; //starts with middle position
               // >= 95 -> counter clockwise
               // <= 85 -> clockwise

void setup(){
SimonKESC.attach(9); //connect this PWM pin with the data cable of the ESC(usually white or yellow)
//motorESC.write(40); //if your ESC doesn`t have the SimonK firmware you can try this; remove the //signs
Serial.begin(115200);
pinMode(3, OUTPUT);      // sets the digital pin as output
}

void loop(){
//int poti = analogRead(potiPin);
//poti = map(poti, 0, 1023, 0, 179);
Serial.println(remote);
SimonKESC.write(remote);
while(!Serial.available()); 
remote = Serial.parseInt();    // Parse an Integer from Serial

if (remote > 90){
  digitalWrite(3, HIGH);  //for oscilloscope
} else {
  digitalWrite(3,LOW);
}
}

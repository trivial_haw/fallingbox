/* 
 * File:   voltage.h
 * Author: Daniel
 *
 * Created on 28. Oktober 2015, 18:04
 */

#ifndef VOLTAGE_H
#define	VOLTAGE_H

//#include <arduino.h>
#include "MovingAverage.h"

class voltage {
public:
    voltage(int pin);
    float getVoltage();
    bool isVoltageOk();
private:
  MovingAverage *average;
  void actualizeVoltage();
  int pin;
  float value;
};

#endif	/* VOLTAGE_H */


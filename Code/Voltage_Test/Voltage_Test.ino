/* Sweep
 by BARRAGAN <http://barraganstudio.com> 
 This example code is in the public domain.

 modified 8 Nov 2013
 by Scott Fitzgerald
 http://arduino.cc/en/Tutorial/Sweep
*/ 

#define AKKUPOWERPIN 17

 
#include "voltage.h"
#include "MovingAverage.h"

voltage *akku;

void setup() 
{ 
akku = new voltage(AKKUPOWERPIN);
Serial.begin(9600);
} 



void loop() 
{

Serial.print("Spannung:  ");
Serial.println(akku->getVoltage());

}
 

#include "eepromGui.h"

eepromGui::eepromGui(){
  this->addr = 0;
  this->startFrame=0;
  this->inputFloat=0;
  this->outputFloat=0;
}

void eepromGui::eprmWrite(){
  Serial.println("Which adress [0,4,8,...]?");
  while(!Serial.available());
  addr=Serial.parseInt();
  
  Serial.println("Value to write [float(4Byte)]?");
  while(!Serial.available());
  inputFloat=Serial.parseFloat();
  
  memcpy(data, &inputFloat, sizeof inputFloat);    // send data
  for(int i=0; i<4; i++){
    EEPROM.write(addr+i, data[i]); 
  }
  Serial.flush();
}

float eepromGui::eprmRead(int adr){ 
  outputFloat=0;
  for( int i=0; i<4; i++){
    data[i]=EEPROM.read(adr+i); 
  }
  memcpy(&outputFloat, data, sizeof outputFloat);// receive data       
}

void eepromGui::eprmClear(){
  for (int i = 0; i < 512; i++)
    EEPROM.write(i, 0);
}

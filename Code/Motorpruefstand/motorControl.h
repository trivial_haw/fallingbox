/* 
 * File:   motorControl.h
 * Author: Daniel
 *
 * Created on 6. Oktober 2015, 19:34
 */

#ifndef MOTORCONTROL_H
#define	MOTORCONTROL_H

#include "Arduino.h"

#define maxValueMotor 255

class motorControl {
public:
    motorControl(int motorInput1,int motorInput2);
    void rotate(float power,int powerDirection);
    void zero();
private:
    int motorInput1;
    int motorInput2;
    int motorPowerOutput;
    bool powerDirection;
};

#endif	/* MOTORCONTROL_H */

#include <Servo.h>

enum {DC, brushless};
enum {LEFT, RIGHT};
enum {CLOCK,COUNTERCLOCK,BOTHSIDE};

#define motorType     brushless     //0-DC Motor 1-Brushless
#define motorDirection CLOCK

#include "motorControl.h"
#include "brushlessControl.h"
#include "math.h"

#define irPin 12

//DC-PINS
#define dcMotorInput1  5     //DC Motor Pins
#define dcMotorInput2  2    //6
#define dcMotorEnable  6    //2
//Brushless-Pin
#define brushlessPin     9     //Brushless

//#define startPWM 94
//#define endPWM   135

float voltage[255];
int endValue =0;

brushlessControl *brushlessMotor;
motorControl *dcMotor;

void setup() {
  Serial.begin(115200);
	if (motorType == DC) {
    pinMode(dcMotorEnable, OUTPUT);  //SET MOTOR PINS
    pinMode(dcMotorInput1, OUTPUT);
    pinMode(dcMotorInput2, OUTPUT);
    dcMotor = new motorControl(dcMotorInput1, dcMotorInput2);
  }
  if (motorType == brushless){
    brushlessMotor = new brushlessControl(brushlessPin);
    brushlessMotor->fwd(90);
    delay(1000);
  }
}
unsigned long time1=0;
unsigned long time2=0;

void loop() {
  if(motorType==brushless){
    switch(motorDirection) {
			case CLOCK:
				startPWM = 95;
				endPWM 	 = 180;
				for(int i = startPWM; i <= endPWM; i++){
					brushlessMotor->fwd(i);
					delay(500);
					Serial.println(i);
					for(int x = 0; x < 50; x++){
						while(!digitalRead(irPin));
						time1=micros();
						if(digitalRead(irPin)==true){
							while(digitalRead(irPin)==true);
						}
						while(!digitalRead(irPin));
						time2=micros();
						voltage[i] += (M_PI * (1 / ((time2-time1)*0.000001)));
					}
					if(i == endPWM){
						for(int a=startPWM; a <= endPWM; a++){
							voltage[a] /= 50;
							Serial.print(a, DEC);
							Serial.print(",");
							Serial.println(voltage[a], DEC);
						}
					}
				}
				break;
			case COUNTERCLOCK:
				startPWM = 85;
				endPWM 	 = 0;
				for(int i = startPWM; i >= endPWM; i--){
					brushlessMotor->fwd(i);
					delay(500);
					Serial.println(i);
					for(int x = 0; x < 50; x++){
						while(!digitalRead(irPin));
						time1=micros();
						if(digitalRead(irPin)==true){
							while(digitalRead(irPin)==true);
						}
						while(!digitalRead(irPin));
						time2=micros();
						voltage[i] += (M_PI * (1 / ((time2-time1)*0.000001)));
					}
					if(i == endPWM){
						for(int a=startPWM; a >= endPWM; a--){
							voltage[a] /= 50;
							Serial.print(a, DEC);
							Serial.print(",");
							Serial.println(voltage[a], DEC);
						}
					}
				}
				break;
			case BOTHSIDE:
				startPWM = 0;
				endPWM 	 = 180;
				for (int i = 90; i >= 0; i-=10) {
					brushlessMotor->fwd(i);
					delay(500);
				}
				for(int i = startPWM; i >= endPWM; i--){
					brushlessMotor->fwd(i);
					delay(500);
					Serial.println(i);
					for(int x = 0; x < 50; x++){
						while(!digitalRead(irPin));
						time1=micros();
						if(digitalRead(irPin)==true){
							while(digitalRead(irPin)==true);
						}
						while(!digitalRead(irPin));
						time2=micros();
						voltage[i] += (M_PI * (1 / ((time2-time1)*0.000001)));
					}
					if(i == endPWM){
						for(int a=startPWM; a >= endPWM; a--){
							voltage[a] /= 50;
							Serial.print(a, DEC);
							Serial.print(",");
							Serial.println(voltage[a], DEC);
						}
					}
				}
		}
    for(int i=startPWM; i<=endPWM; i++){
      brushlessMotor->fwd(i);
      delay(500);
      Serial.println(i);
      for(int x=0; x< 50; x++){
        while(!digitalRead(irPin));
        time1=micros();
        if(digitalRead(irPin)==true){
          while(digitalRead(irPin)==true);
        }
        while(!digitalRead(irPin));
        time2=micros();
        voltage[i] += (M_PI * (1 / ((time2-time1)*0.000001)));
      }
      if(i == endPWM){
        for(int a=startPWM; a <= endPWM; a++){
          voltage[a] /= 50;
          Serial.print(a, DEC);
          Serial.print(",");
          Serial.println(voltage[a], DEC);
        }
      }
    }
  }


  if(motorType==DC){
    for(int i=0; i<255; i++){
      dcMotor->rotate(i, LEFT);
      delay(800);
      for(int x=0; x< 50; x++){
        while(!digitalRead(irPin));
        time1=micros();
        if(digitalRead(irPin)==true){
          while(digitalRead(irPin)==true);
        }
        while(!digitalRead(irPin));
        time2=micros();
        voltage[i] += (2 * M_PI * (1 / (time2-time1)));
      }
      if(i == 255){
        for(int a=0; a < 255; a++){
          voltage[a] /= 50;
          Serial.print(a, DEC);
          Serial.print(",");
          Serial.println(voltage[a], DEC);
        }
      }
    }
  }


  time1 = 0;
  time2 = 0;
  brushlessMotor->fwd(90);
  delay(99999999);
}

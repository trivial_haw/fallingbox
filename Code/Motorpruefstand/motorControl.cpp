/*
 * File:   motorControl.cpp
 * Author: Daniel
 *
 * Created on 6. Oktober 2015, 19:34
 */

#include "motorControl.h"

motorControl::motorControl(int motorInput1,int motorInput2) {
    this->motorInput1=motorInput1;
    this->motorInput2=motorInput2;
    this->powerDirection=0;
    this->motorPowerOutput=0;
}

 void motorControl::rotate(float power,int powerDirection){
    float motorPower=power;
    if (motorPower>100){                                    //Begrenzung der Motorpower auf 100%
    motorPower=100;
    }

   this->motorPowerOutput=(motorPower/100)*maxValueMotor;   //power 0-100% wird umgerechnet nach 0-255
   this->powerDirection=powerDirection;


   if (this->powerDirection==0){              //je nach Drehrichtung wird der Motorcontroler angesteuert
       analogWrite(motorInput1,0);
       analogWrite(motorInput2,motorPowerOutput);
         //Serial.println("Richtung LINKS");
   }
   else{
        analogWrite(motorInput1,motorPowerOutput);
        analogWrite(motorInput2,0);
        //Serial.println("Richtung Rechts");
   }
  }

void motorControl::zero(){
  analogWrite(motorInput1,0);
  analogWrite(motorInput2,0);
  }

#include "brushlessControl.h"

Servo *SimonKESC;

brushlessControl::brushlessControl(int pin) {
  SimonKESC = new Servo;
  this->pwmPin = pin;
  pinMode(pwmPin, OUTPUT);
  SimonKESC->attach(pwmPin);
  SimonKESC->write(90);
  delay(2000);
}

void brushlessControl::fwd(int i) {
  if(i < brushlessZero) i = brushlessZero;
  if(i > brushlessFullSpeed) i = brushlessFullSpeed;
  SimonKESC->write(i);
}

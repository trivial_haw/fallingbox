#include "Arduino.h"
#include <Servo.h>

#ifndef BRUSHLESSCONTROL_H
#define	BRUSHLESSCONTROL_H

#define brushlessZero 40
#define brushlessFullSpeed 135

class brushlessControl {
public:
    brushlessControl(int);
    void fwd(int);

private:
   int pwmPin;
};

#endif	/* BRUSHLESSCONTROL_H */

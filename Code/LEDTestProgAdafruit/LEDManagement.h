#include "Adafruit_NeoPixel.h"

#define NUM_LEDS 4

#define BLACK_COLOR strip.Color(0,0,0)
#define RED_COLOR 	strip.Color(255,0,0)
#define GREEN_COLOR strip.Color(0,255,0)
#define BLUE_COLOR 	strip.Color(0,0,255)
#define WHITE_COLOR strip.Color(255,255,255)

enum colors{
  RED, GREEN, BLUE, WHITE, BLACK, OFF};
enum ledname{
  DEBUG, STATUS, BATTERY, HEARTBEAT};


class LEDManagement {
private:
  Adafruit_NeoPixel strip;
public:
  LEDManagement(uint8_t pin);
  void allLEDsOff();
  void setColor(int ledname, int color);
};

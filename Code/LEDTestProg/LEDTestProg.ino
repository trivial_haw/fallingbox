#include "FastLED.h"
#define NUM_LEDS 4

#define DATA_PIN 5

CRGB leds[NUM_LEDS];

void setup() {
  delay(2000);
  FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);
  FastLED.setBrightness(64);
}
       
void loop() {
   leds[0] = CRGB(255,0,0);
   leds[1] = CRGB(0,255,0);
   leds[2] = CRGB(0,0,255);
   leds[3] = CRGB(255,255,255); 
   FastLED.show();
   delay(1000);
   leds[1] = CRGB(255,0,0);
   leds[2] = CRGB(0,255,0);
   leds[3] = CRGB(0,0,255);
   leds[0] = CRGB(255,255,255); 
   FastLED.show();
   delay(1000);
   leds[2] = CRGB(255,0,0);
   leds[3] = CRGB(0,255,0);
   leds[0] = CRGB(0,0,255);
   leds[1] = CRGB(255,255,255); 
   FastLED.show();
   delay(1000);
   leds[3] = CRGB(255,0,0);
   leds[0] = CRGB(0,255,0);
   leds[1] = CRGB(0,0,255);
   leds[2] = CRGB(255,255,255); 
   FastLED.show();
   delay(1000);
}

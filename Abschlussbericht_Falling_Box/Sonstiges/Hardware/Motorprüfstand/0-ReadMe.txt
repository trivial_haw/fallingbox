Motorkennlinien

Um zu pr�fen, welche Drehzahl bei welchem PWM-Signal erreicht wird, wurden diese Werte mittels eines
Testaufbaus (siehe Konstruktion) aufgenommen. Mittles eines Testprogramms wurden die Geschwindigkeit
der Motoren langsam Schritt f�r Schritt erh�ht und die Werte der Gabellichtschranke aufgenommen.
Diese Werte wurden in einer Excel-Datei visualisiert. Da die ESCs geflasht sind, wurden beide
Drehrichtungen aufgenommen.
In Zukunft k�nnte mit diesen Kennlinien ein Regler ausgelegt werden.
Die Kennlinien in der Excel-Tabelle sind f�r eine bestimmte Schwungmasse (siehe Bilder) aufgenommen worden.
innerH = 50;
motorL=16;          //motor lenght
motorDia=7;         //motor diameter
mTol=0.3;
plateLength = 70;
plateWidth = 40;
plateThickness = 10;
nutLength = 65;
nutThickness = 3;

/*tNut(nutThickness, 10);
#t(nutThickness, 11);
*/


difference(){
  engineBlock();
  translate([35, 15, plateThickness-nutThickness]) tNut(nutThickness, nutLength);
  translate([35, -15, plateThickness-nutThickness]) tNut(nutThickness, nutLength);
}

module engineBlock(){
  $fn=100;
  blockHeiht=innerH/2;
  drillingDepth=blockHeiht-motorDia/2;    //for the M3-Holes

  difference(){
    union(){
      translate([-4,-plateWidth/2,0])cube([plateLength,plateWidth,plateThickness]);
      translate([-4,-plateWidth/2,0])cube([4,plateWidth,innerH-10]);
    }
    for (i = [45:90:405]){
      translate([0,0,innerH/2])
      rotate([i,0,0])
      translate([-2,0,9.5])rotate([0,90,0])#cylinder(r=3/2, h=6,center=true);
    }
    translate([-2,0,innerH/2])rotate([0,90,0])#cylinder(r=12/2 ,h=6 ,center=true);
  }
}

module tNut(thickness, length){
  cube(size=[length, 2*thickness, thickness], center=true);
  translate([0, 0, thickness]) cube(size=[length, thickness, thickness], center=true);
}

module t(thickness, length){
  cube(size=[length, 2*thickness-mTol, thickness-mTol], center=true);
  translate([0, 0, thickness])cube(size=[length, thickness-mTol, thickness+mTol], center=true);
}

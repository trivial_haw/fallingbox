﻿Das Testkreuz war unser erster Prototyp. Auf diesem waren zuerst DC-Motoren montiert, später Brushless-Motoren. 

Da unser erster Ansatz war, PID-Regler für die Ansteuerung der Motoren zu verwenden, wollten wir die Streckenparameter ermitteln um die Regler auslegen 
zu können. 

Hierfür haben wir das Testkreuz in einem Prüfstand montiert (am Anfang horizontal in einer Holzkunstruktion(wie in dem Video zu sehen), später an einem 
Faden vertikal hängend), die Motoren mit einem Sprung angesteuert und die Drehrate des Testkreuzes über die Zeit gemessen. 

Die gemessenen Daten sind die Sprungantwort des Systems, aus denen z.B. mithilfe des Wendetangentenverfahrens die Streckenparameter der IT1-Strecke 
ermittelt werden können. 

Theoretisch kann man dann mit der Strecke und z.B. dem symmetrischen Optimums einen Regler auslegen. Es hat sich nur herausgestellt, dass in dem System 
Nicht-linearitäten (z.B. die Motoren) enthalten sind. Dies erfordert tiefere Kenntnisse der Regelungstechnik als wir sie zu diesem Zeitpunkt hatten. Wir 
haben uns deshalb für eine "BinärRegelung" entschieden (Zuweit in die eine Richtung gedreht, Motor voll im Uhrzeigersinn ansteuern, zuweit in die andere 
Richtung, Motoren voll in die andere Richtung ansteuern).

!!!
Die hier dargestellten Daten waren für ein nicht mehr existierendes System und müssen für jedes System neu ermittelt werden. Die Daten sind nur für die 
Dokumentation gedacht um zu zeigen in welche Richtungen wir gearbeitet haben


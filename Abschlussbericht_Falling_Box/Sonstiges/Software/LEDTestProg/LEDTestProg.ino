#include "LEDManagement.h"
#include <Adafruit_NeoPixel.h>

#define DATA_PIN 5

LEDManagement *led;

void setup() {
  led = new LEDManagement(DATA_PIN);
  delay(2000);
}
       
void loop() {
  led->setColor(HEARTBEAT, GREEN);
}

#include "LEDManagement.h"


LEDManagement::LEDManagement(uint8_t pin) {
  this->strip = Adafruit_NeoPixel(NUM_LEDS, pin, NEO_GRB + NEO_KHZ800);
  strip.begin();
  allLEDsOff();
}

void LEDManagement::allLEDsOff() {
  for(int i=0;i<NUM_LEDS;i++){
    strip.setPixelColor(i, BLACK_COLOR);
  }
  strip.show();
}

void LEDManagement::setColor(int ledname, int color) {
  switch(color) {
  case GREEN: 
    strip.setPixelColor(ledname, GREEN_COLOR); 	
    break;
  case RED: 	
    strip.setPixelColor(ledname, RED_COLOR); 		
    break;
  case BLUE: 	
    strip.setPixelColor(ledname, BLUE_COLOR); 	
    break;
  case WHITE: 
    strip.setPixelColor(ledname, WHITE_COLOR); 	
    break;
  case BLACK: 
    strip.setPixelColor(ledname, BLACK_COLOR); 	
    break;
  case OFF: 	
    strip.setPixelColor(ledname, BLACK_COLOR); 	
    break;
  default: 
    break;
  }
  strip.show();
}


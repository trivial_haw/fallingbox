#include <Servo.h>
#define ROLLPIN   8
#define PITCHPIN  9
#define OSZIPIN   3

//Choose the Motor you want to test --> [PITCH or ROLL]
#define ROLL

Servo SimonKESC;

int remote = 90; //starts with middle position
// 95 >= PWM <= 173 -> clockwise
// 3  <= PWM <= 85  -> counter clockwise
// PWM == 90 -> no rotation and init

void setup(){  
#ifdef PITCH
  SimonKESC.attach(PITCHPIN); 
  Serial.println("Pitchmotor used");
#endif

#ifdef ROLL
  SimonKESC.attach(ROLLPIN);
  Serial.println("Rollmotor used");
#endif
  delay(2000);

  pinMode(OSZIPIN, OUTPUT);
  digitalWrite(OSZIPIN, LOW);

  Serial.begin(115200);
  Serial.println("Init done");
  Serial.print("Actual PWM-Value:   ");
  Serial.println(remote);
}

void loop(){
  if(Serial.available()){
    remote = Serial.parseInt();
    Serial.print("Actual PWM-Value:   ");
    Serial.println(remote);
  }

  if(remote < 3){
    remote = 3;
  }
  if(remote > 173){
    remote = 173;
  }
  SimonKESC.write(remote);

  if (remote > 90){
    digitalWrite(3, HIGH);  //for oscilloscope
  }else {
    digitalWrite(3,LOW);
  }
}


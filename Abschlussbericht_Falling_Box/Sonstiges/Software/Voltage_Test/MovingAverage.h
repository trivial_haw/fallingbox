#ifndef MOVINGAVERAGE_H
#define	MOVINGAVERAGE_H

#include <Arduino.h>

/*Ein Objekt diesr Klasse kann erstellt werden, um einen Mittelwert zu bilden. Es muss die Anzahl n der Samples übergeben werden, die man mitteln moechte.
  Mit AddSample wird der neuste Wert hinzugefühgt und mit getAverage bekommt man den Durchschnitt der letzten n Werte zurueck.*/

class MovingAverage {
public:
    MovingAverage(int n=20);
    
    void AddSample(float newSample);
    float getAverage();
    void reset();
    
private:

  float newSample;
  float *Samples;
  float erg;
  int count;
  int n;

};

#endif	/* MOVINGAVERAGE_H */


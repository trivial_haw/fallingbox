difference(){
  top();
  #fallingBox();
  #infos();
}


module top(){
import("deckel_v2.stl");
}

module fallingBox() {
  translate([-71.5, 0, 3]){
    linear_extrude(height = 2){
      text("The falling-box.", size = 15);
    }
  }
}

module infos() {
  translate([-3, -20, 3]){
    linear_extrude(height = 2){
      text("Bachelorprojekt", size = 6);
      translate([0, -10, 0])text("Mechatronik - WS 15/16", size = 5);
      translate([0, -20, 0])text("Johannes Reichert, Daniel Neutzler,", size = 4);
      translate([0, -26, 0])text("Sven Hartmann, Jonas Winter", size = 4);
    }
  }
}

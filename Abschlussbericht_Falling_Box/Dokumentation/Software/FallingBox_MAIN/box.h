/* Diese Klasse beinhaltet einen Binärregler, welcher die Box in
 * abhängigkeit des Drehwinkels ausrichtet.
 * Die "angle-mapping"-Funktion normiert den aktuellen Winkelwert auf
 * einen 0-360° Wert und verscheibt den Nullpunkt auf den Punkt der
 * gehalten werden soll.
 */

#ifndef BOX_H
#define	BOX_H

#include "Arduino.h"
#include "imu.h"
#include "brushlessControl.h"
#include "defines.h"

class Box {
public:
  Box(imu* imu1);
  void binaryControl(bool);
  void setEnginesToZero();

private:
  double angleMapping(double, bool);
  imu* imu1;
  brushlessControl* pitch;
  brushlessControl* roll;

  double binaryRoll;
  double binaryPitch;
  
  int phiDiff;
  double desiredValue;
};
#endif

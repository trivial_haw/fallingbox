#include "box.h"

Box::Box(imu* imu1){
  this->pitch = new brushlessControl(PITCHPIN);
  this->roll = new brushlessControl(ROLLPIN);
  this->imu1 =imu1;

  desiredValue = DESIREDANGLE ;//Angle we want to reach!!
  phiDiff = 360 - desiredValue;
  
  //SenseroValues for the binaryControl
  binaryRoll=0;        
  binaryPitch=0;
}

void Box::binaryControl(bool debug){
  //Update the SensorValues und Map them ******************************
  imu1->update();
  binaryRoll = imu1->getRollGyro();          //current Values Roll and Pitch
  binaryPitch=imu1->getHeadingGyro();        //BoxPitch = ImuHeading because of Mounting position
  binaryRoll=angleMapping(binaryRoll,debug);     
  binaryPitch=angleMapping(binaryPitch,debug);    
  //********************************************************************

  //BinaryControl - Roll **********************************************
  if (binaryRoll > 0) {
    roll->rotate(debug, BINARY_CLOCKWISE);              //rotate CLOCKWISE
  }
  else if (binaryRoll < 0){
    roll->rotate(debug, BINARY_COUNTERCLOCKWISE);            //rotate COUNTERCLOCKWISE
  }
  else {
    roll->rotate(debug, ZEROSPEED);  //Turn off the Motor
  }
  //********************************************************************

  //BinaryControl - Pitch **********************************************
  if (binaryPitch < 0) {
    pitch->rotate(debug, BINARY_CLOCKWISE);          //rotate CLOCKWISE
  }
  else if (binaryPitch > 0){
    pitch->rotate(debug, BINARY_COUNTERCLOCKWISE);         //rotate COUNTERCLOCKWISE
  }
  else {
    pitch->rotate(debug, ZEROSPEED);    //Turn off the Motor
  }
  //********************************************************************

  if(debug){
    Serial.print("\t Roll: ");
    Serial.print(binaryRoll);
    Serial.print("\t Pitch: ");
    Serial.println(binaryPitch);
  }
}

double Box::angleMapping(double currentVal, bool debug){
  if (((int)currentVal + this->phiDiff) % 360 > 180) {
    return (((int)currentVal + this->phiDiff) % 360) - 360;
  } 
  else {
    return ((int)currentVal + this->phiDiff) % 360;
  }
}

void Box::setEnginesToZero(){
  roll->rotate(false, ZEROSPEED);
  pitch->rotate(false, ZEROSPEED);
}


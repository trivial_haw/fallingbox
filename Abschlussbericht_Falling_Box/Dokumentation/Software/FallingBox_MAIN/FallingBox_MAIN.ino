#include <Adafruit_NeoPixel.h>
#include <Wire.h>
#include <L3G.h>
#include <LSM303.h>
#include <LPS.h>
#include <Servo.h>
#include "LEDManagement.h"
#include "defines.h"
#include "box.h"
#include "voltage.h"
#include "imu.h"

/* -------------------------------------------
* Available Modes of the Box
* 
* Mode Auto  ( nur Schalter1 einschalten): Die Box funktioniert nach dem Aktivity Diagram unter (Dokumentation\Software\Diagrams). Die Box merkt wann Sie fällt und richtet sich automatisch so lange aus bis Sie nicht
* mehr fällt. Zusätzlich dazu ist noch eine Spannungsüberprüfung eingebaut, damit der Lipo nicht tiefenentladen wird.
*   -> Box wartet Status LED ist grün (Zustand Idle)
*   -> Box fällt  Status LED ist blau (Zustand Auto)
*   
* Mode Debug (nur Schalter2 einschalten): Box versucht die lage Roll & Pitch zu regeln. Man kann die Box in der Hand oder auf den Tisch legen und die Drehrichtungen und Auswirkungen der Moren testen. 
*   -> Debug LED ist ROT              (Zustand Debug)
* 
* Mode AkkuSafe : Wird aktiviert wenn der Akku entladen ist. Es passiert nichts mehr.
*   -> Battery LED ist ROT            (Zustand AkkuSave)
*   
* Die LED Heartbeat zeigt an, ob das Programm normal in der Schleife durchläuft. Hängt diese LED hat sich der Controller höchstwarscheinlich aufgehangen.  
* 
* !!!Wichtig!!! Beim einschalten der Box muss diese Horizontal liegen (z.B. auf einem Tisch). Sobalt die Heartbeat LED blinkt kann es los gehen. Der Grund dafür ist, 
* dass sich der IMU initialisieren muss und die "Gyros" den Nullpunkt kennen.
* 
* Schalter 3 und 4 sind noch reserviert für zukünftige Funktionen (z.B. SD Karte Werte aufnehmen).
* -------------------------------------------*/

enum mode {Idle,Auto, Debug, AkkuSafe};
short mode = Idle;  
short oldMode = Idle;
/*-------------------------------------------*/

Box *fallingBox;
voltage *akku;
imu* imu1;
LEDManagement* led;

unsigned long startTime = 0;
unsigned long helpTime = 0;
bool isSerialOn = true;

void setup() {
  Wire.begin();
  //give the I²C sone time to initialize
  delay(20);

  imu1=new imu();
  imu1->initial();
  fallingBox = new Box(imu1);
  akku = new voltage(AKKUPOWERPIN);
  led = new LEDManagement(LEDSTRIPE);

  pinMode(DEBUGLED, OUTPUT);
  pinMode(DEBUGPIN ,INPUT);
  pinMode(AUTOMODEPIN ,INPUT);
  pinMode(PIN3 ,INPUT);
  pinMode(PIN4 ,INPUT);

  pinMode(ONBOARDLED,OUTPUT);
  digitalWrite(ONBOARDLED, HIGH);

  Serial.begin(9600);
  
  startTime =millis();
}

void loop() {
/* -------------------------------------------
 * Let the HeartbeatLED blink to indicate if
 * there is a programmcrash
 * -------------------------------------------*/
  helpTime = millis();
  if(helpTime-startTime > HEARTBEATTIME){
    led->setColor(HEARTBEAT, BLUE);
    startTime = millis();
  }

/* -------------------------------------------
* Update the sensorValues from the IMU
* -------------------------------------------*/
  imu1->update();

/* -------------------------------------------
 * Mode-transition Actions
 * -------------------------------------------*/
  if(digitalRead(AUTOMODEPIN) && imu1->falling()){
    mode = Auto;
    led->allLEDsOff();
  }
 if(digitalRead(AUTOMODEPIN) && !imu1->falling()){
   mode = Idle;
   led->allLEDsOff();
  }
  if(digitalRead(DEBUGPIN)){
    mode = Debug;
    led->allLEDsOff();
  }
  if(!akku->isVoltageOk() && !imu1->falling()){
    mode= AkkuSafe;
    led->allLEDsOff();
  }

/* -------------------------------------------
* Switch the Serial-Communication
* (only in the Debug-Mode)
* -------------------------------------------*/
  if((mode == Debug) && !isSerialOn){
    Serial.begin(9600);
    delay(20);
    isSerialOn = true;
  }
  if((mode == !Debug) && isSerialOn){
    Serial.end();
    isSerialOn = false;
  }

/* -------------------------------------------
 * Mode-bonded Actions
 * Executed in each Loop
 * -------------------------------------------*/
  switch (mode) {
      case Idle:
        led->setColor(STATUS, GREEN);
        fallingBox->setEnginesToZero();
        break;

      case Auto:
       led->setColor(STATUS, BLUE);
        fallingBox->binaryControl(false);
        break;

      case Debug:
        led->setColor(DEBUG, RED);
        Serial.println("Debug");
        fallingBox->binaryControl(true);
        break;

      case AkkuSafe:
        led->setColor(BATTERY, RED);
        break;

      default:
        break;
  }
}

/* This class is responsible for the control of brushless motors .
 * The output is a PWM-Signal.
 * PWM-Value >= 95 counter-clockwise
 * PWM-Value <= 85 clockwise
 * PWM-Value == 90 no rotation
 */

#include "Arduino.h"
#include <Servo.h>
#include "defines.h"

#ifndef BRUSHLESSCONTROL_H
#define	BRUSHLESSCONTROL_H

class brushlessControl {
public:
    brushlessControl(int);
    void rotate(bool, int);

private:
   int pwmPin;
   Servo *SimonKESC;
};

#endif	/* BRUSHLESSCONTROL_H */

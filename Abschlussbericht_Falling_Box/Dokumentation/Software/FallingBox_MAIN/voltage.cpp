#include "voltage.h"

voltage::voltage(int pin) {
  this->pin=pin;
  this->value=0;
  average=new MovingAverage(40);

  int i;                        //for beginning that Voltage isn't 0V because of MovingAverage
  for (i=0;i<50;i++){
    updateVoltage();
  }
}

float voltage::getVoltage(){
  updateVoltage();
  return (average->getAverage());
  }

void voltage::updateVoltage(){
  value=analogRead(pin);
  value=value*0.00967742;     //0-1023   Spannungsteiler 10KOhm zu 5KOhm(am Analogeingang)  868=8,4V  0=0V -> 0.00967742V/dig
  average->AddSample(value);
  }

bool voltage::isVoltageOk(){
  if(getVoltage() <= MINVOLTAGE){
    return false;
  }else{
    return true;
  }
}

#include <Adafruit_NeoPixel.h>

/*Diese Klasse wird zum Ansteuern des LED Streifens genutzt. Mit SetColor kann die Farbe gewaehlt werden. Mit "allLEDsOff" koennen alle ausgeschaltet werden.
  Um Die LEDs passend anzusteuern wird die Adafruit Libary genutzt.*/

/*Anzahl der LEDS muss zum enum ledname passen /Moegliche Farben definieren muss zum enum colors passen*/
#define NUM_LEDS 4

#define BLACK_COLOR strip.Color(0,0,0)
#define RED_COLOR 	strip.Color(255,0,0)
#define GREEN_COLOR strip.Color(0,255,0)
#define BLUE_COLOR 	strip.Color(0,0,255)
#define WHITE_COLOR strip.Color(255,255,255)

enum colors{
  RED, GREEN, BLUE, WHITE, BLACK, OFF};
enum ledname{
  DEBUG, STATUS, BATTERY, HEARTBEAT};

class LEDManagement {
private:
  Adafruit_NeoPixel strip;
public:
  LEDManagement(uint8_t pin);
  void allLEDsOff();
  void setColor(int ledname, int color);
};

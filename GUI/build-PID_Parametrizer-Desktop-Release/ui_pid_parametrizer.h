/********************************************************************************
** Form generated from reading UI file 'pid_parametrizer.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PID_PARAMETRIZER_H
#define UI_PID_PARAMETRIZER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PID_Parametrizer
{
public:
    QWidget *centralWidget;
    QDoubleSpinBox *doubleSpinBox_I_Pitch;
    QLabel *label_3;
    QLabel *label_2;
    QFrame *line_2;
    QLCDNumber *lcd_D_Pitch;
    QLabel *label_7;
    QLabel *label_5;
    QPushButton *shiftButton;
    QLabel *label_6;
    QLabel *label_4;
    QDoubleSpinBox *doubleSpinBox_D_Pitch;
    QDoubleSpinBox *doubleSpinBox_P_Pitch;
    QFrame *line;
    QLCDNumber *lcd_P_Pitch;
    QPushButton *readButton;
    QLabel *label;
    QPushButton *saveButton;
    QLCDNumber *lcd_I_Pitch;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QDoubleSpinBox *doubleSpinBox_P_Roll;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_13;
    QDoubleSpinBox *doubleSpinBox_D_Roll;
    QDoubleSpinBox *doubleSpinBox_I_Roll;
    QLabel *label_14;
    QLabel *label_15;
    QLCDNumber *lcd_D_Roll;
    QLabel *label_16;
    QLCDNumber *lcd_P_Roll;
    QLabel *label_17;
    QLabel *label_18;
    QLCDNumber *lcd_I_Roll;
    QFrame *line_3;
    QFrame *line_4;
    QLabel *label_19;
    QFrame *line_5;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *PID_Parametrizer)
    {
        if (PID_Parametrizer->objectName().isEmpty())
            PID_Parametrizer->setObjectName(QStringLiteral("PID_Parametrizer"));
        PID_Parametrizer->resize(653, 368);
        centralWidget = new QWidget(PID_Parametrizer);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        doubleSpinBox_I_Pitch = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_I_Pitch->setObjectName(QStringLiteral("doubleSpinBox_I_Pitch"));
        doubleSpinBox_I_Pitch->setGeometry(QRect(40, 160, 91, 41));
        doubleSpinBox_I_Pitch->setDecimals(5);
        doubleSpinBox_I_Pitch->setMinimum(-1024);
        doubleSpinBox_I_Pitch->setMaximum(1024);
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(350, 110, 41, 41));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 110, 41, 41));
        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setGeometry(QRect(300, 70, 20, 81));
        line_2->setFrameShadow(QFrame::Sunken);
        line_2->setLineWidth(2);
        line_2->setFrameShape(QFrame::VLine);
        lcd_D_Pitch = new QLCDNumber(centralWidget);
        lcd_D_Pitch->setObjectName(QStringLiteral("lcd_D_Pitch"));
        lcd_D_Pitch->setGeometry(QRect(390, 210, 91, 41));
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(350, 160, 41, 41));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(10, 160, 41, 41));
        shiftButton = new QPushButton(centralWidget);
        shiftButton->setObjectName(QStringLiteral("shiftButton"));
        shiftButton->setGeometry(QRect(290, 160, 41, 27));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(10, 210, 41, 41));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(350, 60, 161, 21));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        label_4->setFont(font);
        doubleSpinBox_D_Pitch = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_D_Pitch->setObjectName(QStringLiteral("doubleSpinBox_D_Pitch"));
        doubleSpinBox_D_Pitch->setGeometry(QRect(40, 210, 91, 41));
        doubleSpinBox_D_Pitch->setDecimals(5);
        doubleSpinBox_D_Pitch->setMinimum(-1024);
        doubleSpinBox_D_Pitch->setMaximum(1024);
        doubleSpinBox_P_Pitch = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_P_Pitch->setObjectName(QStringLiteral("doubleSpinBox_P_Pitch"));
        doubleSpinBox_P_Pitch->setGeometry(QRect(40, 110, 91, 41));
        doubleSpinBox_P_Pitch->setDecimals(5);
        doubleSpinBox_P_Pitch->setMinimum(-1024);
        doubleSpinBox_P_Pitch->setMaximum(1024);
        line = new QFrame(centralWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(300, 190, 20, 91));
        line->setFrameShadow(QFrame::Sunken);
        line->setLineWidth(2);
        line->setFrameShape(QFrame::VLine);
        lcd_P_Pitch = new QLCDNumber(centralWidget);
        lcd_P_Pitch->setObjectName(QStringLiteral("lcd_P_Pitch"));
        lcd_P_Pitch->setGeometry(QRect(390, 110, 91, 41));
        QFont font1;
        font1.setPointSize(8);
        font1.setBold(true);
        font1.setWeight(75);
        lcd_P_Pitch->setFont(font1);
        readButton = new QPushButton(centralWidget);
        readButton->setObjectName(QStringLiteral("readButton"));
        readButton->setGeometry(QRect(370, 260, 101, 31));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 60, 161, 21));
        label->setFont(font);
        saveButton = new QPushButton(centralWidget);
        saveButton->setObjectName(QStringLiteral("saveButton"));
        saveButton->setGeometry(QRect(20, 270, 101, 31));
        saveButton->setCursor(QCursor(Qt::PointingHandCursor));
        saveButton->setMouseTracking(true);
        lcd_I_Pitch = new QLCDNumber(centralWidget);
        lcd_I_Pitch->setObjectName(QStringLiteral("lcd_I_Pitch"));
        lcd_I_Pitch->setGeometry(QRect(390, 160, 91, 41));
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(350, 210, 41, 41));
        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(30, 90, 81, 21));
        QFont font2;
        font2.setBold(true);
        font2.setItalic(true);
        font2.setWeight(75);
        label_9->setFont(font2);
        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(160, 90, 81, 21));
        label_10->setFont(font2);
        doubleSpinBox_P_Roll = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_P_Roll->setObjectName(QStringLiteral("doubleSpinBox_P_Roll"));
        doubleSpinBox_P_Roll->setGeometry(QRect(190, 110, 91, 41));
        doubleSpinBox_P_Roll->setDecimals(5);
        doubleSpinBox_P_Roll->setMinimum(-1024);
        doubleSpinBox_P_Roll->setMaximum(1024);
        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(160, 110, 41, 41));
        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(160, 160, 41, 41));
        label_13 = new QLabel(centralWidget);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(160, 210, 41, 41));
        doubleSpinBox_D_Roll = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_D_Roll->setObjectName(QStringLiteral("doubleSpinBox_D_Roll"));
        doubleSpinBox_D_Roll->setGeometry(QRect(190, 210, 91, 41));
        doubleSpinBox_D_Roll->setDecimals(5);
        doubleSpinBox_D_Roll->setMinimum(-1024);
        doubleSpinBox_D_Roll->setMaximum(1024);
        doubleSpinBox_I_Roll = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_I_Roll->setObjectName(QStringLiteral("doubleSpinBox_I_Roll"));
        doubleSpinBox_I_Roll->setGeometry(QRect(190, 160, 91, 41));
        doubleSpinBox_I_Roll->setDecimals(5);
        doubleSpinBox_I_Roll->setMinimum(-1024);
        doubleSpinBox_I_Roll->setMaximum(1024);
        label_14 = new QLabel(centralWidget);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(360, 90, 81, 21));
        label_14->setFont(font2);
        label_15 = new QLabel(centralWidget);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setGeometry(QRect(500, 90, 81, 21));
        label_15->setFont(font2);
        lcd_D_Roll = new QLCDNumber(centralWidget);
        lcd_D_Roll->setObjectName(QStringLiteral("lcd_D_Roll"));
        lcd_D_Roll->setGeometry(QRect(550, 210, 91, 41));
        label_16 = new QLabel(centralWidget);
        label_16->setObjectName(QStringLiteral("label_16"));
        label_16->setGeometry(QRect(510, 210, 41, 41));
        lcd_P_Roll = new QLCDNumber(centralWidget);
        lcd_P_Roll->setObjectName(QStringLiteral("lcd_P_Roll"));
        lcd_P_Roll->setGeometry(QRect(550, 110, 91, 41));
        lcd_P_Roll->setFont(font1);
        label_17 = new QLabel(centralWidget);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setGeometry(QRect(510, 110, 41, 41));
        label_18 = new QLabel(centralWidget);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setGeometry(QRect(510, 160, 41, 41));
        lcd_I_Roll = new QLCDNumber(centralWidget);
        lcd_I_Roll->setObjectName(QStringLiteral("lcd_I_Roll"));
        lcd_I_Roll->setGeometry(QRect(550, 160, 91, 41));
        line_3 = new QFrame(centralWidget);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setGeometry(QRect(140, 90, 20, 171));
        line_3->setFrameShape(QFrame::VLine);
        line_3->setFrameShadow(QFrame::Sunken);
        line_4 = new QFrame(centralWidget);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setGeometry(QRect(480, 90, 20, 171));
        line_4->setFrameShape(QFrame::VLine);
        line_4->setFrameShadow(QFrame::Sunken);
        label_19 = new QLabel(centralWidget);
        label_19->setObjectName(QStringLiteral("label_19"));
        label_19->setGeometry(QRect(10, -10, 1041, 61));
        QFont font3;
        font3.setPointSize(22);
        font3.setBold(true);
        font3.setItalic(true);
        font3.setWeight(75);
        label_19->setFont(font3);
        line_5 = new QFrame(centralWidget);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setGeometry(QRect(10, 40, 631, 16));
        line_5->setFrameShape(QFrame::HLine);
        line_5->setFrameShadow(QFrame::Sunken);
        PID_Parametrizer->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(PID_Parametrizer);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 653, 25));
        PID_Parametrizer->setMenuBar(menuBar);
        mainToolBar = new QToolBar(PID_Parametrizer);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        PID_Parametrizer->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(PID_Parametrizer);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        PID_Parametrizer->setStatusBar(statusBar);
#ifndef QT_NO_SHORTCUT
        label_5->setBuddy(label_5);
        label_12->setBuddy(label_5);
#endif // QT_NO_SHORTCUT

        retranslateUi(PID_Parametrizer);

        QMetaObject::connectSlotsByName(PID_Parametrizer);
    } // setupUi

    void retranslateUi(QMainWindow *PID_Parametrizer)
    {
        PID_Parametrizer->setWindowTitle(QApplication::translate("PID_Parametrizer", "PID_Parametrizer", 0));
        label_3->setText(QApplication::translate("PID_Parametrizer", "P", 0));
        label_2->setText(QApplication::translate("PID_Parametrizer", "P", 0));
        label_7->setText(QApplication::translate("PID_Parametrizer", "I", 0));
        label_5->setText(QApplication::translate("PID_Parametrizer", "I", 0));
        shiftButton->setText(QApplication::translate("PID_Parametrizer", "<<", 0));
        label_6->setText(QApplication::translate("PID_Parametrizer", "D", 0));
        label_4->setText(QApplication::translate("PID_Parametrizer", "Read from EEPROM", 0));
        readButton->setText(QApplication::translate("PID_Parametrizer", "Read  Values", 0));
        label->setText(QApplication::translate("PID_Parametrizer", "Write in EEPROM", 0));
        saveButton->setText(QApplication::translate("PID_Parametrizer", "Save Values", 0));
        label_8->setText(QApplication::translate("PID_Parametrizer", "D", 0));
        label_9->setText(QApplication::translate("PID_Parametrizer", "Pitch", 0));
        label_10->setText(QApplication::translate("PID_Parametrizer", "Roll", 0));
        label_11->setText(QApplication::translate("PID_Parametrizer", "P", 0));
        label_12->setText(QApplication::translate("PID_Parametrizer", "I", 0));
        label_13->setText(QApplication::translate("PID_Parametrizer", "D", 0));
        label_14->setText(QApplication::translate("PID_Parametrizer", "Pitch", 0));
        label_15->setText(QApplication::translate("PID_Parametrizer", "Roll", 0));
        label_16->setText(QApplication::translate("PID_Parametrizer", "D", 0));
        label_17->setText(QApplication::translate("PID_Parametrizer", "P", 0));
        label_18->setText(QApplication::translate("PID_Parametrizer", "I", 0));
        label_19->setText(QApplication::translate("PID_Parametrizer", "The Falling-Box - PID-Parametrizer", 0));
    } // retranslateUi

};

namespace Ui {
    class PID_Parametrizer: public Ui_PID_Parametrizer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PID_PARAMETRIZER_H

#ifndef PID_PARAMETRIZER_H
#define PID_PARAMETRIZER_H

#include <QMainWindow>
#include <QDialog>
//For Arduino
#include <QMessageBox>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDebug>
#include <QtCore>
#include <QtGui>
#include <QMessageBox>
#include <string>
#include <QMainWindow>

namespace Ui {
class PID_Parametrizer;
}

class PID_Parametrizer : public QMainWindow
{
    Q_OBJECT

public:
    explicit PID_Parametrizer(QWidget *parent = 0);
    ~PID_Parametrizer();

private slots:
    //Arduino Stuff
    void readFloats();
    void writeByte(char);
    void writeFloat(float);
    void on_readButton_clicked();
    void on_saveButton_clicked();
    void on_shiftButton_clicked();

    void on_saveButton_released();

private:
    Ui::PID_Parametrizer *ui;
    void initArduino();
    float p_roll,i_roll,d_roll;
    float p_pitch,i_pitch,d_pitch;
    //Arduino-Stuff
    QSerialPort * arduino;
    static const quint16 arduino_uno_vendor_id = 5824; //UNO: 9025;//teensY: 5824;
    static const quint16 arduino_uno_product_id = 1155; //UNO: 67;//teensY: 1155;
    QByteArray serialData;
    QString serialBuffer;
    QString parsed_data;
};

#endif // PID_PARAMETRIZER_H

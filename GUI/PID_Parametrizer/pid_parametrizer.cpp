#include "pid_parametrizer.h"
#include "ui_pid_parametrizer.h"
union float2bytes { float f; char b[sizeof(float)]; };

PID_Parametrizer::PID_Parametrizer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PID_Parametrizer)
{
    ui->setupUi(this);
    this->initArduino();
    p_roll=0;
    i_roll=0;
    d_roll=0;
    p_pitch=0;
    i_pitch=0;
    d_pitch=0;
}

PID_Parametrizer::~PID_Parametrizer()
{
    delete ui;
    if(arduino->isOpen()){
        arduino->close();       //Close the serial port if it's open.
    }
}

void PID_Parametrizer::initArduino(){
    //ARDUINO - STUFF
        //FORM HERE
    arduino = new QSerialPort(this);
    serialBuffer = "";
    parsed_data = "";
/*
    qDebug() << "Number of ports: " << QSerialPortInfo::availablePorts().length() << "\n";
    foreach(const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts()){
        qDebug() << "Description: " << serialPortInfo.description() << "\n";
        qDebug() << "Has vendor id?: " << serialPortInfo.hasVendorIdentifier() << "\n";
        qDebug() << "Vendor ID: " << serialPortInfo.vendorIdentifier() << "\n";
        qDebug() << "Has product id?: " << serialPortInfo.hasProductIdentifier() << "\n";
        qDebug() << "Product ID: " << serialPortInfo.productIdentifier() << "\n";
    }
/**/
    bool arduino_is_available = false;
    QString arduino_uno_port_name;

    //  For each available serial port
    foreach(const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts()){
        //  check if the serialport has both a product identifier and a vendor identifier
        if(serialPortInfo.hasProductIdentifier() && serialPortInfo.hasVendorIdentifier()){
            //  check if the product ID and the vendor ID match those of the arduino uno
            if((serialPortInfo.productIdentifier() == arduino_uno_product_id)
                    && (serialPortInfo.vendorIdentifier() == arduino_uno_vendor_id)){
                arduino_is_available = true; //    arduino uno is available on this port
                arduino_uno_port_name = serialPortInfo.portName();
            }
        }
    }
    if(arduino_is_available){
        qDebug() << "Arduino found at Port: "<<arduino_uno_port_name<<"n";
        arduino->setPortName(arduino_uno_port_name);
        arduino->open(QSerialPort::ReadWrite);
        arduino->setBaudRate(QSerialPort::Baud115200);
        arduino->setDataBits(QSerialPort::Data8);
        arduino->setFlowControl(QSerialPort::NoFlowControl);
        arduino->setParity(QSerialPort::NoParity);
        arduino->setStopBits(QSerialPort::OneStop);

        //Have I really to do ths????
        QObject::connect(arduino, SIGNAL(readyRead()), this, SLOT(readFloats()));
        //

        arduino->clear();
    }else{
        qDebug() << "Couldn't find the correct port for the arduino.\n";
        QMessageBox::information(this, "Serial Port Error", "Couldn't open serial port to arduino.");
    }
    //TO HERE

    ui->statusBar->showMessage(
          QString("Port:   %1") .arg(arduino_uno_port_name));
}

void PID_Parametrizer::writeFloat(float value){
    float2bytes f2b;
    f2b.f=value;
    arduino->write(f2b.b, 4);
}

void PID_Parametrizer::writeByte(char value){
    arduino->write(&value, 1);
}

void PID_Parametrizer::readFloats(){
   char received[4];
   char at=0;
   float2bytes f2bP_Pitch, f2bI_Pitch, f2bD_Pitch;
   float2bytes f2bP_Roll, f2bI_Roll, f2bD_Roll;
   if(arduino->bytesAvailable() >= 25){
        arduino->read(&at, 1);
        if(at == '@'){
            for(int i=0; i<4; i++){
                arduino->read(&received[i], 1);
                f2bP_Pitch.b[i] = (unsigned char)(received[i]);
            }
            for(int i=0; i<4; i++){
                arduino->read(&received[i], 1);
                f2bI_Pitch.b[i] = (unsigned char)(received[i]);
            }
            for(int i=0; i<4; i++){
                arduino->read(&received[i], 1);
                f2bD_Pitch.b[i] = (unsigned char)(received[i]);
            }
            for(int i=0; i<4; i++){
                arduino->read(&received[i], 1);
                f2bP_Roll.b[i] = (unsigned char)(received[i]);
            }
            for(int i=0; i<4; i++){
                arduino->read(&received[i], 1);
                f2bI_Roll.b[i] = (unsigned char)(received[i]);
            }
            for(int i=0; i<4; i++){
                arduino->read(&received[i], 1);
                f2bD_Roll.b[i] = (unsigned char)(received[i]);
            }
        }
        arduino->clear();
        qDebug() << "Pitch" << f2bP_Pitch.f << "\t" << f2bI_Pitch.f << "\t" << f2bD_Pitch.f << endl;
        qDebug() << "Roll" << f2bP_Roll.f << "\t" << f2bI_Roll.f << "\t" << f2bD_Roll.f << endl;
        p_pitch = f2bP_Pitch.f;
        i_pitch = f2bI_Pitch.f;
        d_pitch = f2bD_Pitch.f;
        p_roll = f2bP_Roll.f;
        i_roll = f2bI_Roll.f;
        d_roll = f2bD_Roll.f;
    }
}


void PID_Parametrizer::on_readButton_clicked()
{
    readFloats();
    ui->lcd_P_Pitch->display(this->p_pitch);
    ui->lcd_I_Pitch->display(this->i_pitch);
    ui->lcd_D_Pitch->display(this->d_pitch);
    ui->lcd_P_Roll->display(this->p_roll);
    ui->lcd_I_Roll->display(this->i_roll);
    ui->lcd_D_Roll->display(this->d_roll);
}

void PID_Parametrizer::on_saveButton_clicked()
{
    p_pitch=ui->doubleSpinBox_P_Pitch->value();
    i_pitch=ui->doubleSpinBox_I_Pitch->value();
    d_pitch=ui->doubleSpinBox_D_Pitch->value();
    p_roll=ui->doubleSpinBox_P_Roll->value();
    i_roll=ui->doubleSpinBox_I_Roll->value();
    d_roll=ui->doubleSpinBox_D_Roll->value();

    //qDebug() << p_roll << "\t" << i_roll << "\t" << d_roll << endl;
    writeByte('@');
    writeFloat(p_pitch);
    writeFloat(i_pitch);
    writeFloat(d_pitch);
    writeFloat(p_roll);
    writeFloat(i_roll);
    writeFloat(d_roll);

    writeByte('@');
    writeFloat(p_pitch);
    writeFloat(i_pitch);
    writeFloat(d_pitch);
    writeFloat(p_roll);
    writeFloat(i_roll);
    writeFloat(d_roll);

}

void PID_Parametrizer::on_shiftButton_clicked()
{
    ui->doubleSpinBox_P_Pitch->setValue(ui->lcd_P_Pitch->value());
    ui->doubleSpinBox_I_Pitch->setValue(ui->lcd_I_Pitch->value());
    ui->doubleSpinBox_D_Pitch->setValue(ui->lcd_D_Pitch->value());
    ui->doubleSpinBox_P_Roll->setValue(ui->lcd_P_Roll->value());
    ui->doubleSpinBox_I_Roll->setValue(ui->lcd_I_Roll->value());
    ui->doubleSpinBox_D_Roll->setValue(ui->lcd_D_Roll->value());
}

void PID_Parametrizer::on_saveButton_released()
{
}

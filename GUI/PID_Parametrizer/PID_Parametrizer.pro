#-------------------------------------------------
#
# Project created by QtCreator 2015-12-14T16:59:48
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PID_Parametrizer
TEMPLATE = app


SOURCES += main.cpp\
        pid_parametrizer.cpp

HEADERS  += pid_parametrizer.h

FORMS    += pid_parametrizer.ui
